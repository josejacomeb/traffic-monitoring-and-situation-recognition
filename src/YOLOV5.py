#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
# Imports of YOLOV5

import os
import random
import logging

import cv2
from yolov5 import YOLOv5  # Using high level Library
import yolov5.utils.plots

logger = logging.getLogger(__name__)


class YOLOV5:

    def __init__(self, model, precision=0.25, device="cuda", images_dir=None):
        self.device = device
        self.model_path = model
        self.model_name = self.model_path.split("/")[-4]
        # init yolov5 model
        self.yolov5 = None
        self.images_dir = images_dir
        self.preds_dir = None
        self.precision = precision
        if self.images_dir:
            self.preds_dir = os.path.join(images_dir, "preds_{}/".format(self.model_name))
            if not os.path.exists(self.preds_dir):
                logger.info("Creating a directory to save the data: {}".format(self.preds_dir))
                os.mkdir(self.preds_dir)
        self.model_classes = ['car', 'pedestrian', 'motorcycle', 'bicycle', 'bus', 'truck']
        self.colors = [[random.randint(0, 255) for _ in range(3)] for _ in self.model_classes]

    def infer(self, image, image_name=None):
        """
		@params Return the bounding boxes and the image
		"""

        results = self.yolov5.predict(image)
        cpu_tensor = results.pred[0].cpu()
        tensor_2_numpy = cpu_tensor.numpy()
        # TODO Beautify this line
        line = ""
        unified_format_detections = []
        for detection in tensor_2_numpy:
            if detection[-2] > self.precision:
                formatted_detection = {}
                x1 = int(detection[0])
                x2 = int(detection[2])
                y1 = int(detection[1])
                y2 = int(detection[3])
                x1, x2 = min(x1, x2), max(x1, x2)
                y1, y2 = min(y1, y2), max(y1, y2)
                formatted_detection["xyx2y2"] = (x1, y1, x2, y2)
                formatted_detection["center"] = (int(x1 + (x2 - x1) / 2), int(y1 + (y2 - y1) / 2.0))
                class_id = int(detection[-1])
                formatted_detection["class"] = self.model_classes[int(detection[-1])]
                formatted_detection["confidence"] = detection[-2]
                # yolov5.utils.plots.plot_one_box(formatted_detection["xyx2y2"], image, self.colors[class_id], formatted_detection["class"])
                unified_format_detections.append(formatted_detection)
                if self.images_dir:
                    # class_name confidence left top right bottom
                    # Bbox typeType xyx2y2
                    line += "{} {} {} {} {} {}".format(self.model_classes[int(class_id)], formatted_detection["confidence"],
                                                       formatted_detection["xyx2y2"][0], formatted_detection["xyx2y2"][1],
                                                       formatted_detection["xyx2y2"][2], formatted_detection["xyx2y2"][3])
                    line += "\n"
        if self.images_dir:
            image_basename = os.path.basename(image_name).split(".")[0]
            pred_image_path = os.path.join(self.preds_dir, "{}.txt".format(image_basename))

            with open(pred_image_path, "w") as file:
                file.write(line)
        return unified_format_detections

    def load_model(self):
        logger.info("Initializing yolo Model")
        self.yolov5 = YOLOv5(self.model_path, self.device)
