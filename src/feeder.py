#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
import os
import random
import logging

import cv2
import mimetypes

import csv
import numpy as np
# Get the AP
from sklearn import metrics
import logging

logger = logging.getLogger(__name__)
mimetypes.init()


class Feeder:
    """
    Returns the frame from the desired type of file
    """

    def __init__(self, filepath, out_path=None, dataset_csv_path=None):
        mimetypes.init()
        mimestart = mimetypes.guess_type(filepath)[0]
        self.type = 'video'  # video, dataset or camera
        self.wait = 1  # 1 if video, 30 if dataset
        self.files = []  # List of files inside the dataset folder
        self.index = 0  # Index of the file vector
        self.manual_idx = False  # Stop the adding in the feeder if the user set it manually
        self.cap = None  # Instance of the Video Capture
        self.frame = None
        self.frame_rate = 30  # Frame rate of the video/dataset
        self.frame_name = ""  # Use this variable to save the predictions result
        self.filepath = ""
        self.lineType = cv2.LINE_AA
        self.thickness = 1
        self.color = (255, 0, 120)
        self.fontFace = 1
        self.fontScale = 1
        self.writing_video = False
        self.width = 0
        self.height = 0
        self.dataset_csv_path = dataset_csv_path
        self.model_classes = ['car', 'pedestrians', 'motorcycle', 'bicycle', 'bus', 'truck',
                              'unidentifiable']
        self.classes_colours = {
            "car": (94, 222, 240),
            'pedestrian': (130, 141, 108),
            'motorcycle': (70, 108, 185),
            'bicycle': (56, 51, 154),
            'bus': (33, 32, 52),
            'truck': (70, 48, 38)
        }
        if self.dataset_csv_path:
            if os.path.isfile(self.dataset_csv_path):
                self.alpha = 0.2
                self.illustrative_frame = None
                self.load_dataset_csv()

        if mimestart == 'image':
            raise UserWarning('It is not allowed to use an image')
        if filepath.isnumeric():
            self.type = 'camera'
            self.cap = cv2.VideoCapture(int(filepath))
            self.filename = "camera_{}".format(filepath)
            self.frame_rate = int(self.cap.get(cv2.CAP_PROP_FPS))
        if os.path.isfile(filepath):
            self.type = 'video'
            self.wait = 30
            self.cap = cv2.VideoCapture(filepath)
            self.filename = "video_{}".format(os.path.basename(filepath))
            self.frame_rate = int(self.cap.get(cv2.CAP_PROP_FPS))
        elif os.path.isdir(filepath):
            self.type = 'dataset'
            self.wait = 30
            self.filename = "{}".format(os.path.basename(os.path.normpath(filepath)))
            files = os.listdir(filepath)  # Get files of the folder
            logger.info("Listing the files in the {} directory".format(filepath))
            for file in files:
                if os.path.isfile(filepath + file) and "image" in mimetypes.guess_type(file)[0]:
                    self.files.append(file)
            # sort the list
            self.files.sort()
            logger.info("Finished listing the files")
        self.wait = int(1000 / self.frame_rate)
        self.filepath = filepath
        if out_path != "":
            ret, frame = self.get_next_frame()
            self.height, self.width = frame.shape[0:2]
            self.set_index(0)
            self.writing_video = True
            fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
            self.out_video = cv2.VideoWriter(out_path, fourcc, 10, (self.width, self.height))

    def get_next_frame(self):
        '''
        Get the next frame of the desired input
        '''
        ret = False
        if self.type == 'camera' or self.type == 'video':
            self.index = int(self.cap.get(cv2.CAP_PROP_POS_FRAMES))
            self.frame_name = "{}_{}".format(self.type, self.index)
            ret, self.frame = self.cap.read()
        else:
            logger.debug("Total Frames: {} Current Idx: {} ".format(len(self.files), self.index))
            if self.index < len(self.files):
                logger.debug(self.filepath + self.files[self.index])
                self.frame_name = self.files[self.index]
                self.frame = cv2.imread(self.filepath + self.files[self.index])
                if not self.manual_idx:
                    self.index += 1
                else:
                    self.manual_idx = False
                ret = True
            else:
                ret = False
                self.frame = None
        # Be sure a non-empty matrix is returned
        if self.frame is None:
            ret = False
            self.height, self.width = (0, 0)
        else:
            ret = True
            self.height, self.width = self.frame.shape[0:2]

        return ret, self.frame

    def set_index(self, index=0):
        if self.type == 'camera' or self.type == 'video':
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, index)
        else:
            self.index = index
            self.manual_idx = True
            logger.debug("Idx: {}, manual_idx: {}".format(self.index, self.manual_idx))

    def load_dataset_csv(self):
        with open(self.dataset_csv_path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                dict_row = dict(row)
                basename = os.path.basename(dict_row['filename'])
                if not basename in self.dataset_bboxes.keys():
                    self.dataset_bboxes[basename] = []
                bbox = dict()
                bbox["class"] = dict_row["class"]
                bbox["xmin"] = dict_row["xmin"]
                bbox["xmax"] = dict_row["xmax"]
                bbox["ymin"] = dict_row["ymin"]
                bbox["ymax"] = dict_row["ymax"]
                self.dataset_bboxes[basename].append(bbox)

    def generate_random_color(self):
        color = [int(255 * random.random()) for _ in range(3)]
        return color

    # Based on YOLOV5 libraries https://github.com/ultralytics/yolov5/blob/master/utils/plots.py
    def draw_detections(self, detections, img, line_thickness=3, points_track_draw=10):
        # Plots one bounding box on image img
        for detection in detections:
            tl = line_thickness or round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1  # line/font thickness
            color = self.classes_colours[detection["class"]]
            c1, c2 = (detection["xyx2y2"][0], detection["xyx2y2"][1]), (detection["xyx2y2"][2], detection["xyx2y2"][3])
            cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
            label = detection["class"]
            tf = max(tl - 1, 1)  # font thickness
            t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
            c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
            cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)  # filled
            cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)
            color_id = detection["id_color"]
            cv2.drawMarker(img, detection["kalman_center"], color_id, cv2.MARKER_DIAMOND, 30, tl)
            if "id" in detection:
                text = str(detection["id"])
                t_size = cv2.getTextSize(text, 0, fontScale=tl / 3, thickness=tf)[0]
                c2 = (detection["xyx2y2"][2], detection["xyx2y2"][3])
                c1 = c2[0] - t_size[0], c2[1]
                c2 = c2[0], c2[1] + t_size[1] + 3
                cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)  # filled
                cv2.putText(img, text, (c1[0], c1[1] + 2 + t_size[1]), 0, tl / 3, [225, 255, 255], thickness=tf,
                            lineType=cv2.LINE_AA)
                n_past_locations = len(detection["past_locations"])
                draw_every_n_points = 1 if n_past_locations < points_track_draw else int(
                    n_past_locations / points_track_draw)
                for number in range(0, n_past_locations, draw_every_n_points):
                    center_draw = detection["past_locations"][number]["past_center"]
                    cv2.circle(img, center_draw, 2 * tl, color_id, -1)
                    if number > 0:
                        cv2.line(img, center_draw, previous_center, color_id, tl, cv2.LINE_8)
                    previous_center = center_draw

    def write_text(self, frame, text, position, fontface=cv2.FONT_HERSHEY_SIMPLEX, fontscale=1,
                   color=(255, 15, 255), thickness=1, linetype=cv2.LINE_AA):
        cv2.putText(frame, text, position, fontface, fontscale, color, thickness, linetype)
        return frame

    def write_mat(self, filename, img):
        if img is not None:
            cv2.imwrite(filename, img)

    def write_video(self, frame):
        if self.writing_video:
            logger.info("Writing video")
            self.out_video.write(frame)
