#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""

import logging
import math
import random
import time

import cv2  # Add KCF tracker and CSRT
import dlib  # Correlation tracker
import numpy as np
from munkres import Munkres  # Algorithm to assign the trackers to the detection

logger = logging.getLogger(__name__)
# from sort.sort import Sort #Improve the package characteristics


class KalmanBoxTracker:
    def __init__(self, bbox):
        """
        @params a
        """
        std_acc = 0.75
        x_std_meas = 0.01
        y_std_meas = 0.01
        xc = int(bbox[0] + (bbox[2] - bbox[0])/2.)
        yc = int(bbox[1] + (bbox[3] - bbox[1])/2.)
        self.dt = 1/10.

        self.transitionMatrix = np.array(  # A matrix
            [
                [1.0, 0.0, self.dt, 0.0],
                [0.0, 1.0, 0, self.dt],
                [0.0, 0.0, 1, 0.0],
                [0.0, 0.0, 0.0, 1],
            ]
        )
        self.measurementMatrix = 1.0 * np.array(
            [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0]]
        )  # H Matrix
        self.processNoiseCov = (  # Q Matrix
                np.array(
                    [
                        [(self.dt ** 4) / 4.0, 0.0, (self.dt ** 3) / 2.0, 0.0],
                        [0.0, (self.dt ** 4) / 4.0, 0.0, (self.dt ** 3) / 2.0],
                        [(self.dt ** 3) / 2.0, 0.0, self.dt ** 2.0, 0.0],
                        [0.0, (self.dt ** 3) / 2.0, 0.0, self.dt ** 2],
                    ]
                )
                * std_acc
        )
        self.measurementNoiseCov = np.array(  # Matrix R
            [[x_std_meas ** 2, 0.0], [0.0, y_std_meas ** 2]]
        )
        self.errorCovPost = 1.0 * np.ones((4, 4))

        self.statePost = np.array([[xc], [yc], [0.0], [0.0]])  # P Matrix
        self.controlMatrix = np.matrix(
            [
                [(self.dt ** 2) / 2, 0],  # B Matrix
                [0, (self.dt ** 2) / 2],
                [self.dt, 0],
                [0, self.dt],
            ]
        )
        self.kf = cv2.KalmanFilter(self.transitionMatrix.shape[0], self.measurementMatrix.shape[0],
                                   self.controlMatrix.shape[-1])
        self.kf.transitionMatrix = self.transitionMatrix  # A matrix
        self.kf.measurementMatrix = self.measurementMatrix  # H Matrix
        self.kf.processNoiseCov = self.processNoiseCov  # Q Matrix
        self.kf.measurementNoiseCov = self.measurementNoiseCov  # Matrix R
        self.kf.errorCovPost = self.errorCovPost
        # Initialize the first point
        self.kf.statePost = self.statePost  # P Matrix
        self.kf.controlMatrix = self.controlMatrix  # B Matrix

        self.width = 0
        self.height = 0

    def update(self, bbox):
        """
    Updates the state vector with observed bbox.
    """
        xc = int(bbox[0] + (bbox[2] - bbox[0]) / 2.)
        yc = int(bbox[1] + (bbox[3] - bbox[1]) / 2.)
        measurement_matrix = 1. * np.array([[xc], [yc]])
        self.kf.correct(measurement_matrix)
        self.width = abs(bbox[2] - bbox[0])
        self.height = abs(bbox[3] - bbox[1 ])

    def predict(self, img=None):
        """
    Advances the state vector and returns the predicted bounding box estimate.
    """
        prediction = self.kf.predict()
        half_width = self.width / 2.
        half_height = self.height / 2.
        bbox = (int(prediction[0] - half_width), int(prediction[1] - half_height), int(prediction[0] + half_width), int(prediction[1] + half_height))
        return bbox