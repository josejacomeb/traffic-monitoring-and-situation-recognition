#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
import os
import logging

import tensorflow as tf
import numpy as np

logger = logging.getLogger(__name__)

class TF_OD:

    def __init__(self, model, precision, images_dir=None):
        self.model = model
        self.precision = precision
        self.detect_fn = None
        self.label_id_offset = 1
        self.images_dir = images_dir
        self.model_name = self.model.split("/")[-3]
        self.category_index = {
            1: {'id': 1, 'name': 'car'},
            2: {'id': 2, 'name': 'pedestrian'},
            3: {'id': 3, 'name': 'motorcycle'},
            4: {'id': 4, 'name': 'bicycle'},
            5: {'id': 5, 'name': 'bus'},
            6: {'id': 6, 'name': 'truck'}
        }
        if self.images_dir:
            self.preds_dir = os.path.join(images_dir, "preds_{}/".format(self.model_name))
            if not os.path.exists(self.preds_dir):
                logger.info("Creating a directory to save the data: {}".format(self.preds_dir))
                os.mkdir(self.preds_dir)

    def infer(self, image, image_name=None):
        """
            @params Return the bounding boxes and the image
        """
        input_tensor = np.expand_dims(image, 0)
        detections = self.detect_fn(input_tensor)
        image_with_detections = image.copy()
        image_height, image_width = image.shape[:2]
        unified_format_detections = []
        """viz_utils.visualize_boxes_and_labels_on_image_array(
            image_with_detections,
            detections['detection_boxes'][0].numpy(),
            detections['detection_classes'][0].numpy().astype(np.int32),
            detections['detection_scores'][0].numpy(),
            self.category_index,
            use_normalized_coordinates=True,
            max_boxes_to_draw=200,
            min_score_thresh=self.precision,
            agnostic_mode=False)"""
        line = ""
        for idx, detection_score in enumerate(detections['detection_scores'][0].numpy()):
            if detection_score > self.precision:
                detection = {}
                detection["class"] = self.category_index[int(detections['detection_classes'][0][idx])]['name']
                detection["confidence"] = detection_score
                # Bbox typeType xyx2y2 x and y are the mins
                x1 = int(detections["detection_boxes"][0][idx][1] * image_width)
                x2 = int(detections["detection_boxes"][0][idx][3] * image_width)
                y1 = int(detections["detection_boxes"][0][idx][0] * image_height)
                y2 = int(detections["detection_boxes"][0][idx][2] * image_height)
                #Set p1 as minimun
                x1, x2 = min(x1, x2), max(x1, x2)
                y1, y2 = min(y1, y2), max(y1, y2)
                detection["xyx2y2"] = (x1, y1, x2, y2)
                detection["center"] = (int(x1 + (x2 - x1)/2.0), int(y1 + (y2 - y1)/2.0))
                unified_format_detections.append(detection)
                if self.images_dir:
                    # class_name confidence left top right bottom
                    line += "{} {} {} {} {} {}".format(detection["class"], detection["confidence"],
                                                       detection["xyx2y2"][0], detection["xyx2y2"][1],
                                                       detection["xyx2y2"][2], detection["xyx2y2"][3])
                    line += "\n"
            if self.images_dir:
                image_basename = os.path.basename(image_name).split(".")[0]
                pred_image_path = os.path.join(self.preds_dir, "{}.txt".format(image_basename))
                with open(pred_image_path, "w") as file:
                    file.write(line)

        return unified_format_detections

    def load_model(self):
        tf.keras.backend.clear_session()
        self.detect_fn = tf.saved_model.load(self.model)
