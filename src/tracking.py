#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""

import logging
import math
import random
import time

import cv2  # Add KCF tracker and CSRT
import dlib  # Correlation tracker
import numpy as np
from munkres import Munkres  # Algorithm to assign the trackers to the detection
#Try with this library
from src.kalman_tracker_opencv import KalmanBoxTracker

logger = logging.getLogger(__name__)
# from sort.sort import Sort #Improve the package characteristics


class Tracker:

    def __init__(self, tracker_name, min_iou=0.10, max_elements=100, points_to_track=10, kalman=True, dt=0.016, kta=3,
                 debug=True):

        """
        @params a
        """
        # https://www.pyimagesearch.com/2018/08/06/tracking-multiple-objects-with-opencv/
        self.tracker_name = tracker_name
        self.min_iou = min_iou
        self.maximum = 1e5  # Maximun value for the cost matrix
        self.points_to_track = points_to_track
        self.supported_trackers = {
            "correlation": dlib.correlation_tracker,
            "csrt": cv2.TrackerCSRT_create,
            "kcf": cv2.TrackerKCF_create,
            #            "sort": Sort
        }
        logger.info("Using {} tracker name".format(tracker_name))
        self.trackers = {}
        self.trackers_id = [i for i in range(max_elements)]  # Generate max ids per element
        self.dt = dt
        self.keep_tracking_alive = kta #Cycles to keep tracking alive
        if self.tracker_name not in self.supported_trackers.keys():
            raise NameError("Please specify the correct name of the tracker")
        if debug:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        self.correlation_threshold = 10
        self.kalman = kalman
        self.mot_dict = [] #Save in MOT16 format


    def update(self, image):
        """
        Update the tracker's position
        """
        dict_detections = []
        for tracker_id in self.trackers.keys():
            dict_detection = {}
            x1, y1, x2, y2 = 0, 0, 0, 0  # x1y1x2y2 format
            success = True
            #Predict the location via the Kalman filter
            kalman_prediction = self.trackers[tracker_id]["kalman"]["kalman_tracker"].predict()
            self.trackers[tracker_id]["kalman"]["kalman_prediction"] = kalman_prediction
            if self.tracker_name == "correlation":
                # Send the new image to track
                confidence = self.trackers[tracker_id]["tracker"].update(image)
                # Get the position
                pos = self.trackers[tracker_id]["tracker"].get_position()
                success = False if pos.is_empty() or confidence < self.correlation_threshold else True
                # unpack the position object
                x1, y1 = int(pos.left()), int(pos.top())
                x2, y2 = int(pos.right()), int(pos.bottom())
            elif self.tracker_name == "csrt" or self.tracker_name == "kcf":
                logger.debug("Update: {}".format(image.shape))
                success, bbox = self.trackers[tracker_id]["tracker"].update(image)
                logger.debug("Tracker id: {} Success: {} Bbox: {}".format(tracker_id, success, bbox))
                (x, y, w, h) = [int(v) for v in bbox]
                x1, y1 = x, y
                x2, y2 = x + w, y + h
            elif "Another tracker":
                logger.debug("Implement here another tracker")
                pass
            if not success:
                logger.debug("Tracker {} failed to track the object!".format(tracker_id))
                #If the object can't be tracked, use the Kalman bbox to replac eit
                self.trackers[tracker_id]["tracking?"] = success
                if self.kalman:
                    x1, y1, x2, y2 = (int(v) for v in kalman_prediction)

            self.trackers[tracker_id]["xyx2y2"] = (x1, y1, x2, y2)
            past_center = self.trackers[tracker_id]["center"]
            self.trackers[tracker_id]["past_locations"].append({"past_center": past_center, "time": time.time()})
            if len(self.trackers[tracker_id]["past_locations"]) > self.points_to_track:
                #Use just 10 points spaced of the total trajectory
                final_past_locations = []
                first_element_time = self.trackers[tracker_id]["past_locations"][0]["time"]
                end_element_time = self.trackers[tracker_id]["past_locations"][-1]["time"]
                spaced_time_increments = (end_element_time - first_element_time) / (self.points_to_track - 1)
                logger.debug("Spaced_time_increments = {}, first: {}, last: {}".format(spaced_time_increments, first_element_time, end_element_time))
                #Preserve the first element of the original list
                final_past_locations.append(self.trackers[tracker_id]["past_locations"][0])
                #Check to the center of the range the minimum distance
                start_interval = 1  # start searching at 1
                for center_range in np.arange(first_element_time + spaced_time_increments, end_element_time, spaced_time_increments):
                    minimum_candidate = []
                    for i in range(start_interval, len(self.trackers[tracker_id]["past_locations"])):
                        past_location = self.trackers[tracker_id]["past_locations"][i]
                        time_difference = abs(past_location["time"] - center_range)
                        if time_difference <= spaced_time_increments:
                            minimum_candidate.append({"past_location": past_location, "time_difference": time_difference, "i": i})
                    if minimum_candidate:
                        sorted(minimum_candidate, key=lambda minimum_candidate: minimum_candidate["time_difference"])
                        start_interval = minimum_candidate[0]["i"]
                        final_past_locations.append(minimum_candidate[0]["past_location"]) #Minimun distance
                # Preserve the end element of the list
                final_past_locations.append(self.trackers[tracker_id]["past_locations"][-1])
                self.trackers[tracker_id]["past_locations"] = final_past_locations

            object_center = (int(x1 + (x2 - x1) / 2.0), int(y1 + (y2 - y1) / 2.0))
            kalman_center = (int(kalman_prediction[0] + (kalman_prediction[2] - kalman_prediction[0])/2.),
                             int(kalman_prediction[1] + (kalman_prediction[3] - kalman_prediction[1])/2.))
            self.trackers[tracker_id]["center"] = object_center
            #Check if it's touching the corners of the image
            image_height, image_width = image.shape[0:2]
            logger.debug("Tracker id: {} image_width: {}, image_height: {} x1: {}, x2: {}, y1: {}, y2: {}".format(tracker_id, image_width, image_height, x1, x2, y1, y2))
            #Check if bbox is touching the limits of the image
            if x2 <= image_width or x1 >= 0 or y1 >= 0 or y2 <= image_height:
                dict_detection["xyx2y2"] = self.trackers[tracker_id]["xyx2y2"]
                dict_detection["class"] = self.trackers[tracker_id]["class"]
                dict_detection["id"] = self.trackers[tracker_id]["id"]
                dict_detection["center"] = self.trackers[tracker_id]["center"]
                dict_detection["past_locations"] = self.trackers[tracker_id]["past_locations"]
                dict_detection["id_color"] = self.trackers[tracker_id]["id_color"]
                dict_detection["kalman_center"] = kalman_center #Kalman Center
                dict_detections.append(dict_detection)
        return dict_detections

    def add(self, image, dict_detections):
        """
            Add the image to the tracker
        """
        # If tracker is empty, populate with data
        if not self.trackers:
            logger.debug("Populating trackers!")
            for detection in dict_detections:
                tracker_number, tracker_dict = self.generate_tracker_dict(detection, image)
                self.trackers[tracker_number] = tracker_dict
        elif len(dict_detections) == 0:
            # If not detections, restart the tracker until new detections arrive
            self.trackers.clear()
        else:
            # Implementation of the Hungarian Algorithm
            munkresmatrix = {"cost_matrix": [], "dets_tracker_pairs": []}
            logger.debug("Len Trackers: {} Len Detections: {}".format(len(self.trackers), len(dict_detections)))
            ids_detections_to_track = []  # ID detections in a list to add to a tracker
            for idx, detection in enumerate(dict_detections):
                # Detection columns
                column_trackers = []
                detection_tracker_pair = []  # Real pairs of tracker and detections
                for tracker_id in self.trackers.keys():
                    # Distance between the centers
                    center_x_det = detection["center"][0]
                    center_x_tracker = self.trackers[tracker_id]["center"][0]
                    center_y_det = detection["center"][1]
                    center_y_tracker = self.trackers[tracker_id]["center"][1]
                    iou = self.calculate_iou(detection["xyx2y2"], self.trackers[tracker_id]["xyx2y2"])
                    distance_two_centers = self.maximum  # Set as maximum to make calculation easier
                    if iou > self.min_iou:
                        distance_two_centers = math.sqrt((center_x_tracker - center_x_det) ** 2 +
                                                         (center_y_tracker - center_y_det) ** 2)
                    column_trackers.append(distance_two_centers)
                    detection_tracker_pair.append((idx, tracker_id))

                    logger.debug("Tracker ID: {} Detection ID: {} Distance: {} IoU: {}".format(tracker_id,
                                                                                        idx, distance_two_centers, iou))
                # Detections rows
                logger.debug("Column Trackers: {} Detection_tracker_pair: {}".format(column_trackers, detection_tracker_pair))
                num_maximums = 0
                for column in column_trackers:
                    if column == self.maximum:
                        num_maximums += 1
                if num_maximums < len(column_trackers):
                    munkresmatrix["cost_matrix"].append(column_trackers)
                    munkresmatrix["dets_tracker_pairs"].append(detection_tracker_pair)
                else:
                    ids_detections_to_track.append(idx)
                    logger.debug("logger.debug ids detections to track {}".format(ids_detections_to_track))
            if len(ids_detections_to_track) < len(dict_detections):  # If there're not IoU
                logger.debug("Munkres Matrix: {}".format(munkresmatrix))
                # Calculate Hungarian Algorithm and assign
                m = Munkres()
                results_hungarian_algorithm = m.compute(munkresmatrix["cost_matrix"])
                logger.debug("Result hungarian algorithm: {}".format(results_hungarian_algorithm))
                tracker_ids = list(self.trackers.keys())
                for idx, tracker_corresponding_id in enumerate(self.trackers.keys()):
                    tracker_ids[idx] = tracker_corresponding_id
                for result in results_hungarian_algorithm:
                    # Get the detection id and the tracker id
                    detection_id, tracker_id = munkresmatrix["dets_tracker_pairs"][result[0]][result[1]]
                    logger.debug("Associating the tracker id: {} with the detection {}".format(tracker_id, detection_id))
                    _, self.trackers[tracker_id] = self.generate_tracker_dict(dict_detections[detection_id], image,
                                                                              tracker_id)

                    tracker_ids.remove(tracker_id)  # Check the tracker as located by the algorithm

                logger.debug("New ids to track".format(ids_detections_to_track))

                # Delete old trackers
                image_height, image_width = image.shape[0:2]
                for tracker_id in tracker_ids:
                    x1 = self.trackers[tracker_id]["xyx2y2"][0]
                    x2 = self.trackers[tracker_id]["xyx2y2"][1]
                    y1 = self.trackers[tracker_id]["xyx2y2"][2]
                    y2 = self.trackers[tracker_id]["xyx2y2"][3]
                    in_frame = (x1 > 0 and x2 < image_width and y1 > 0 and y2 < image_height)
                    if self.trackers[tracker_id]["kalman"]["lost_by_n_cycles"] < self.keep_tracking_alive and in_frame:
                        self.trackers[tracker_id]["kalman"]["lost_by_n_cycles"] += 1
                    else:
                        logger.debug("Removing tracker with ID: {}".format(tracker_id))
                        self.trackers.pop(tracker_id)
                        self.trackers_id.append(tracker_id)
            # Add new detections to track
            for id_detection in ids_detections_to_track:
                id, dict_tracker = self.generate_tracker_dict(dict_detections[id_detection], image)
                logger.debug("Adding the detection {} to a new tracker {}".format(id_detection, id))
                self.trackers[dict_tracker["id"]] = dict_tracker
        return dict_detections

    def generate_random_color(self):
        color = [int(255 * random.random()) for _ in range(3)]
        return color

    def generate_tracker_dict(self, detection, image, current_id=None):
        """
        Generate  a tracker dict to initialize with the
        """
        dict_tracker_info = {}
        xc, yc = detection["center"]  # Center
        if current_id is None:
            tracker = self.supported_trackers[self.tracker_name]()
            dict_tracker_info["tracker"] = tracker
            dict_tracker_info["id_color"] = self.generate_random_color()
            dict_tracker_info["past_locations"] = []  # Save the past location of the object
            # Create the Kalman filter instance
            dict_tracker_info["kalman"] = {
                "kalman_tracker": KalmanBoxTracker(detection["xyx2y2"]),
                "lost_by_n_cycles": 0,
                "kalman_prediction": [],
                "kalman_bbox": ()
            }
        else:
            # Correct after data association
            self.trackers[current_id]["kalman"]["kalman_tracker"].update(detection["xyx2y2"])
            dict_tracker_info = self.trackers[current_id]
        if self.tracker_name == "correlation":
            bbox = dlib.rectangle(detection["xyx2y2"][0], detection["xyx2y2"][1], detection["xyx2y2"][2],
                                  detection["xyx2y2"][3])
            dict_tracker_info["tracker"].start_track(image, bbox)
        elif self.tracker_name == "kcf" or self.tracker_name == "csrt":
            # For opencv the bbox should be (x, y, w, h)
            x = detection["xyx2y2"][0]
            y = detection["xyx2y2"][1]
            w = abs(detection["xyx2y2"][2] - detection["xyx2y2"][0])
            h = abs(detection["xyx2y2"][3] - detection["xyx2y2"][1])
            bbox = (x, y, w, h)
            logger.debug("{} Image shape: {} opencv bbox: {} bbox_xyx2y2: {}".format(current_id, image.shape, bbox, detection["xyx2y2"]))
            dict_tracker_info["tracker"].init(image, bbox)
        elif self.tracker_name == "sort":
            bbox = None #Set until it can be fixxed
            dict_tracker_info["tracker"].update(image, bbox)

        idx = current_id if current_id is not None else self.trackers_id.pop(0)  # Get the id from a list of 0 - n maximum ids
        dict_tracker_info["id"] = idx
        dict_tracker_info["class"] = detection["class"]
        detection["id"] = idx
        detection["id_color"] = dict_tracker_info["id_color"]
        detection["past_locations"] = dict_tracker_info["past_locations"]
        if current_id is not None:
            past_center = dict_tracker_info["center"] = detection["center"]
            dict_tracker_info["past_locations"].append({"past_center": past_center, "time": time.time()})

        dict_tracker_info["center"] = detection["center"]
        detection["kalman_center"] = detection["center"] #Reinit the center
        dict_tracker_info["xyx2y2"] = detection["xyx2y2"]
        dict_tracker_info["confidence"] = detection["confidence"]
        dict_tracker_info["tracking?"] = True #Defines if the object is being tracked or have lost the track
        return idx, dict_tracker_info

    def calculate_iou(self, boxA, boxB):
        # Based on https://www.pyimagesearch.com/2016/11/07/intersection-over-union-iou-for-object-detection/
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2], boxB[2])
        yB = min(boxA[3], boxB[3])
        # compute the area of intersection rectangle
        interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
        boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)
        # return the intersection over union value
        return iou

    def save_results(self, detection_dict):
        for detection in detection_dict:
            frame_number = int(detection["frame_name"].split(".")[0])
            detection_id = detection["id"]
            confidence = 0.0
            x1 = detection["xyx2y2"][0]
            y1 = detection["xyx2y2"][1]
            width = abs(detection["xyx2y2"][2] - detection["xyx2y2"][0])
            height = abs(detection["xyx2y2"][3] - detection["xyx2y2"][1])
            mot_line = "{},{},{},{},{},{},{},-1,-1".format(frame_number, detection_id, x1, y1, width, height, confidence)
            self.mot_dict.append(mot_line)
