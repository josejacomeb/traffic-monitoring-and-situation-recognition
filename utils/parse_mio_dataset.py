#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import argparse
import csv
import random
import os
import numpy as np

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
'''
Formatted Data
filename, label, x, y, height, width
'''


class ParseMIO:
    """
        Small Python Utility to Parse a MIO dataset, stored in csv and convert it to a TF OD csv
    """

    def __init__(self, dataset_filename="", number_test_images=10,
                 labels_to_include="", default_extension=".jpg", number_of_digits=6):
        self.dataset_filename = dataset_filename
        self.dataset_path = os.path.dirname(self.dataset_filename)
        self.labels_to_include = labels_to_include
        self.number_test_images = int(number_test_images)
        self.number_of_digits = number_of_digits
        self.default_extension = default_extension
        self.dataset = []


    def read_csv(self):
        self.dataset = []
        with open(self.dataset_filename, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            counter = 0
            for row in csv_reader:
                class_name = ""
                if "_" in row[1]:
                    class_name = row[1]#.split("_")[-1] #Split to get the real cathegory
                else:
                    class_name = row[1]
                if class_name in self.labels_to_include:
                    counter += 1
                    tf_od_dict = {'filename': "", 'width': 9, 'height': 0, 'class': 'abc', 'xmin': 0, 'ymin': 0,
                                  'xmax': 0, 'ymax': 0}
                    if counter%1000 == 0:
                        print("Counter: {}".format(counter))
                    row[0] = self.dataset_path + '/train/' + row[0] + self.default_extension
                    tf_od_dict['filename'] = row[0]
                    img = cv2.imread(row[0])
                    (height, width) = img.shape[:2]
                    tf_od_dict['width'] = width
                    tf_od_dict['height'] = height
                    tf_od_dict['class'] = row[1]
                    tf_od_dict['xmin'] = row[2]
                    tf_od_dict['ymin'] = row[3]
                    tf_od_dict['xmax'] = row[4]
                    tf_od_dict['ymax'] = row[5]
                    self.dataset.append(tf_od_dict)

    def save_csv(self, output_name):
        #TF Object detection csv format ['filename','width','height','class','xmin','ymin','xmax','ymax'] not normalized
        print("Starting the saving process")
        with open(output_name, mode='w') as csv_file:
            fieldnames = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(self.dataset)
        print("File saved as {}".format(output_name))


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", '--csv', help="Path to the Dataset csv file ", required=True, default="")
    parser.add_argument("-l", '--labels', help="Labels to include inside the final dataset",
                        default="articulated_truck,bicycle,bus,car,motorcycle,motorized_vehicle,non-motorized_vehicle,"
                                "pedestrian,pickup_truck,single_unit_truck,work_van")
    parser.add_argument("-n", '--number', help="Number of test images to show", type=int, default=10)
    parser.add_argument("-o", '--output', help="Output name of the csv file with the extension .csv",
                        default="output.csv")
    return parser


def main():
    args = build_argparser().parse_args()  # Get the args from the command line
    if not os.path.isfile(args.csv):
        print("Please specify an appropriate input file")
        return -1

    parseMIO = ParseMIO(args.csv, labels_to_include=args.labels)
    parseMIO.read_csv()
    parseMIO.save_csv(output_name=args.output)


if __name__ == "__main__":
    main()
