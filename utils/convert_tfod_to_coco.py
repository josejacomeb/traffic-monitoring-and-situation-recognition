#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import json
import pandas as pd
import argparse

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
'''
Small Script to convert a TF OD Dataset to COCO format
Based on https://gist.github.com/PallawiSinghal/e11c4b77bcd608f481206884845e3efc#file-tensorflow2coco-py 
'''




def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv", '-c', help="File Path to the TF OD csv dataset", type=str, required=True)
    parser.add_argument("--output", '-o', help="Output folder to save the COCO dataset", type=str, required=True)

    return parser


def main():
    args = build_argparser().parse_args()
    path = args.csv
    save_json_path = args.output

    data = pd.read_csv(path)

    images = []
    categories = []
    annotations = []

    category = {}
    category["supercategory"] = 'none'
    category["id"] = 0
    category["name"] = 'None'
    categories.append(category)

    data['fileid'] = data['filename'].astype('category').cat.codes
    data['categoryid'] = pd.Categorical(data['class'], ordered=True).codes
    data['categoryid'] = data['categoryid'] + 1
    data['annid'] = data.index
    def image(row):
        image = {}
        image["height"] = row.height
        image["width"] = row.width
        image["id"] = row.fileid
        image["file_name"] = row.filename
        return image

    def category(row):
        category = {}
        category["supercategory"] = 'None'
        category["id"] = row.categoryid
        category["name"] = row[4]
        return category

    def annotation(row):
        annotation = {}
        area = (row.xmax - row.xmin) * (row.ymax - row.ymin)
        annotation["segmentation"] = []
        annotation["iscrowd"] = 0
        annotation["area"] = area
        annotation["image_id"] = row.fileid

        annotation["bbox"] = [row.xmin, row.ymin, row.xmax - row.xmin, row.ymax - row.ymin]

        annotation["category_id"] = row.categoryid
        annotation["id"] = row.annid
        return annotation

    for row in data.itertuples():
        annotations.append(annotation(row))

    imagedf = data.drop_duplicates(subset=['fileid']).sort_values(by='fileid')
    for row in imagedf.itertuples():
        images.append(image(row))

    catdf = data.drop_duplicates(subset=['categoryid']).sort_values(by='categoryid')
    for row in catdf.itertuples():
        categories.append(category(row))

    data_coco = {}
    data_coco["images"] = images
    data_coco["categories"] = categories
    data_coco["annotations"] = annotations

    json.dump(data_coco, open(save_json_path, "w"), indent=4)


if __name__ == "__main__":
    main()
