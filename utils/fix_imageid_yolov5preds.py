#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import json

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""


class FixYOLOV5Preds:
    """
        Small Python Utility replace the image_id(actually the file_name of the dataset) with the correct image_id
        according to the COCO Dataset Format and fix the minimun score of the objects
    """

    def __init__(self, ann_file, pred_file, minscore):
        self.ann_file = ann_file
        self.pred_file = pred_file
        self.ann_dict = None
        self.pred_dict = None
        self.minscore = float(minscore)

    def fix_json(self):
        with open(self.ann_file) as f:
            self.ann_dict = json.load(f)
        with open(self.pred_file) as f:
            self.pred_dict = json.load(f)
        elements_to_delete = []
        for idx, pred in enumerate(self.pred_dict):
            value_to_search = pred["image_id"]
            if pred["score"] > self.minscore:
                for image in self.ann_dict["images"]:
                    if image["file_name"].split(".")[0] == value_to_search:
                        self.pred_dict[idx]["image_id"] = image["id"]
                        self.pred_dict[idx]["category_id"] += 1
                        break
            else:
                elements_to_delete.append(pred)
        for element in elements_to_delete:
            self.pred_dict.remove(element)
        dir_name = os.path.dirname(self.pred_file)
        base_name = os.path.basename(self.pred_file)
        base_name = "{}_{}.json".format(base_name.split(".")[0], "fix")
        filepath = os.path.join(dir_name, base_name)
        print("File saved in: {}".format(filepath))
        with open(filepath, 'w') as json_file:
            json.dump(self.pred_dict, json_file)


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--ann", '-a', help="Filename ", type=str, required=True)
    parser.add_argument("--pred", '-pred', help="Directory of the annotations", required=True)
    parser.add_argument("--minscore", '-m', help="Min score for the bboxes", default=0.2)
    return parser


def main():
    args = build_argparser().parse_args()
    if not os.path.isfile(args.ann):
        print("Please, provide a filepath for the annotation file")
        return -1
    if not os.path.isfile(args.pred):
        print("Please, provide a filepath for the prediction file")
        return -1

    fixYOLOv5 = FixYOLOV5Preds(args.ann, args.pred, args.minscore)
    fixYOLOv5.fix_json()


if __name__ == "__main__":
    main()
