#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import argparse
import json
import random
import os
import numpy as np
import csv
from pycocotools.coco import COCO

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
'''
Formatted Data
Format
'''


class ParseAAU:
    """
        Small Python Utility to Parse a AAU Rain Snow dataset, stored in csv and convert it to a TF OD csv
    """

    def __init__(self, dataset_filename="", dataset_path=None, output_name=None, number_test_images=10):
        self.dataset_filename = dataset_filename
        self.dataset_path = ''
        if dataset_path:
            self.dataset_path = dataset_path
        else:
            self.dataset_path = os.path.dirname(self.dataset_filename)
        self.number_test_images = int(number_test_images)
        self.dataset = []
        self.output_name = ""
        if output_name:
            self.output_name = output_name
        else:
            self.output_name = os.path.basename(self.dataset_filename).split(".")[0] + ".csv"

    def read_json(self):
        self.dataset = []
        # font
        font = cv2.FONT_HERSHEY_SIMPLEX
        # fontscale
        fontscale = 1
        # Blue color in BGR
        color = (255, 0, 0)
        coco = COCO(self.dataset_filename)
        elements = coco.loadCats(coco.getCatIds())

        labels = [element['name'] for element in elements]
        catIds = coco.getCatIds(catNms=['pedestrian', 'car', 'motorcycle', 'bicycle', 'bus', 'truck']);
        id = 0
        with open(self.dataset_filename) as json_file:
            raw_data = json.load(json_file)
            length_dataset = len(raw_data['images'])
            visualize_images = []
            for i in range(self.number_test_images):
                visualize_images.append(int(random.random()*(length_dataset - 1)))
            for i in range(length_dataset):
                id = raw_data['images'][i]['id']
                bboxes = []
                annIds = coco.getAnnIds(imgIds=id, catIds=catIds, iscrowd=None)
                anns = coco.loadAnns(annIds)
                for ann in anns:
                    tf_od_dict = {'filename': "", 'width': 0, 'height': 0, 'class': 'abc', 'xmin': 0, 'ymin': 0,
                                  'xmax': 0, 'ymax': 0}
                    tf_od_dict['width'] = raw_data['images'][i]['width']
                    tf_od_dict['height'] = raw_data['images'][i]['height']
                    tf_od_dict['filename'] = raw_data['images'][i]['file_name']
                    tf_od_dict['class'] = labels[ann['category_id'] - 1]
                    xmin = int(ann['bbox'][0])
                    xmax = int(ann['bbox'][0] + ann['bbox'][2])
                    ymin = int(ann['bbox'][1])
                    ymax = int(ann['bbox'][1] + ann['bbox'][3])
                    aux = 0
                    if xmin > xmax:
                        aux = xmin
                        xmin = xmax
                        xmax = aux
                    aux = 0
                    if ymin > ymax:
                        aux = ymin
                        ymin = ymax
                        ymax = aux

                    tf_od_dict['xmin'] = xmin
                    tf_od_dict['ymin'] = ymin
                    tf_od_dict['xmax'] = xmax
                    tf_od_dict['ymax'] = ymax
                    if ann['bbox'][2] > 0 or ann['bbox'][3] > 0:
                        self.dataset.append(tf_od_dict)
                        bboxes.append({'bbox': [xmin, ymin, xmax, ymax], 'class': labels[ann['category_id'] - 1]})
                for j in visualize_images:
                    if i == j:
                        img = cv2.imread("{}/{}".format(self.dataset_path, tf_od_dict['filename']))
                        for bbox in bboxes:
                            color = ((random.random()*255), (random.random()*255), (random.random()*255))
                            pt1 = (int(bbox['bbox'][0]), int(bbox['bbox'][1]))
                            pt2 = (int(bbox['bbox'][2]), int(bbox['bbox'][3]))
                            cv2.rectangle(img, pt1, pt2, color, 4)
                            cv2.putText(img, bbox['class'], (int(bbox['bbox'][0]), int(bbox['bbox'][1])), font,
                                        fontscale, color, 1, cv2.LINE_AA)
                        cv2.imshow("Test_Image_{}".format(j), img)
                        cv2.waitKey()
        sorted(self.dataset, key=lambda i: i.get('filename'))


    def save_csv(self):
        #TF Object detection csv format ['filename','width','height','class','xmin','ymin','xmax','ymax'] not normalized
        print("Starting the saving process")
        with open(self.output_name, mode='w') as csv_file:
            fieldnames = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(self.dataset)
        print("File saved as {}".format(self.output_name))


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-j", '--json', help="Path to the json file ", required=True, default="")
    parser.add_argument("-d", "--dataset_path", help="Path to the dataset file", default=None)
    parser.add_argument("-o", '--output', help="Output name of the csv file with the extension .csv",
                        default=None)
    return parser


def main():
    args = build_argparser().parse_args()  # Get the args from the command line
    if not os.path.isfile(args.json):
        print("Please specify an appropriate input file")
        return -1

    ParseAAUDataset = ParseAAU(args.json, args.dataset_path, args.output)
    ParseAAUDataset.read_json()
    ParseAAUDataset.save_csv()


if __name__ == "__main__":
    main()
