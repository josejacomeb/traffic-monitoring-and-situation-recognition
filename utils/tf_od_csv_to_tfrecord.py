"""
Usage:
  # From tensorflow/models/
  # Create train data:
  python polytrack_csv_to_tfrecord.py --csv_input=data/train_labels.csv --image_dir=/path/to/dataset_images/ --output_path=train.record
  # Create test data:
  python polytrack_csv_to_tfrecord.py --csv_input=data/test_labels.csv --image_dir=/path/to/dataset_images/ --output_path=test.record
  #Based on: https://blog.roboflow.ai/how-to-create-to-a-tfrecord-file-for-computer-vision/
"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import

import os
import io
import sys
import pandas as pd
import tensorflow as tf

from PIL import Image
from object_detection.utils import dataset_util
from collections import namedtuple, OrderedDict
import argparse

def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", '--csv_input', help="Path to the CSV input e.g. /path/to/name.csv", required=True)
    parser.add_argument("-o", '--output_path', help="Path to output TFRecord e.g. /path/to/TFRecord", required=True)
    parser.add_argument("-i", '--image_dir', help="Path to images e.g. /path/to/dataset_images/", required=True)
    return parser

# TO-DO replace this with label map
def class_text_to_int(row_label):

    if row_label == 'car':
        return 1
    elif row_label == 'pedestrian':
        return 2
    elif row_label == 'motorcycle':
        return 3
    elif row_label == 'bicycle':
        return 4
    elif row_label == 'bus':
        return 5
    elif row_label == 'truck':
        return 6
    else:
        None


def split(df, group):
    data = namedtuple('data', ['filename', 'object'])
    gb = df.groupby(group)
    return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]


def create_tf_example(group, path):
    with tf.io.gfile.GFile(os.path.join(path, '{}'.format(group.filename)), 'rb') as fid:
        encoded_image_data = fid.read()
    encoded_image = io.BytesIO(encoded_image_data)
    image = Image.open(encoded_image)
    width, height = image.size

    filename = group.filename.encode('utf8')
    image_format = group.filename.split(".")[-1].encode('utf8')

    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []
    classes_text = []
    classes = []

    for index, row in group.object.iterrows():
        xmins.append(row['xmin'] / width)
        xmaxs.append(row['xmax'] / width)
        ymins.append(row['ymin'] / height)
        ymaxs.append(row['ymax'] / height)
        classes_text.append(row['class'].encode('utf8'))
        classes.append(class_text_to_int(row['class']))
    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_image_data),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example


def main(_):
    args = build_argparser().parse_args()  # Get the args from the command line
    writer = tf.io.TFRecordWriter(args.output_path)
    path = os.path.join(args.image_dir)
    examples = pd.read_csv(args.csv_input)
    grouped = split(examples, 'filename')
    for group in grouped:
        tf_example = create_tf_example(group, path)
        writer.write(tf_example.SerializeToString())

    writer.close()
    output_path = os.path.join(os.getcwd(), args.output_path)
    print('Successfully created the TFRecords: {}'.format(output_path))


if __name__ == '__main__':
    tf.compat.v1.app.run()
