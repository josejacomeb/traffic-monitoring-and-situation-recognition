#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import argparse
import csv
import os
import shutil  # Copy files

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""


class ConvertDatasetYAML:
    """
        Small Python Utility to convert a TF OD csv Dataset to a YoloV5 YAML PyTorch dataset
    """

    def __init__(self, filepath, output_dir, dataset_dir, type, labels):
        self.dataset = []
        self.filepath = filepath
        self.output_dir = output_dir
        self.dataset_dir = dataset_dir
        self.type = type
        self.destination_folders = {"train": "train", "test": "test", "val": "valid"}
        self.destination_folder = self.destination_folders[type]
        self.YOLOV5YAMLExt = "txt"
        self.labels = labels
        self.image_folder = "images"
        self.labels_folder = "labels"

    def read_csv(self):
        self.dataset = []
        with open(self.filepath, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for index, row in enumerate(csv_reader):
                if index > 0:
                    self.dataset.append(row)

    def convertDataset(self):
        length_dataset = len(self.dataset)
        for index, dataset_file in enumerate(self.dataset):
            image_path = os.path.join(self.dataset_dir, dataset_file[0])
            name_of_file = "{}_{}".format(os.path.split(image_path)[0].split("/")[-1],
                                          os.path.basename(image_path).split(".")[0])
            image_extension = dataset_file[0].split(".")[-1]
            object_class = self.labels[dataset_file[3]]
            x_min = float(dataset_file[4])
            x_max = float(dataset_file[6])
            y_min = float(dataset_file[5])
            y_max = float(dataset_file[7])
            image_width = float(dataset_file[1])
            image_height = float(dataset_file[2])
            object_width = abs(x_max - x_min)
            object_height = abs(y_max - y_min)
            x_center = (x_min + (object_width / 2.0))
            y_center = (y_min + (object_height / 2.0))
            object_height /= image_height
            object_width /= image_width
            x_center /= image_width
            y_center /= image_height

            line = "{} {:.16f} {:.16f} {:.16f} {:.16f}\n".format(object_class, x_center, y_center, object_width,
                                                                 object_height)
            new_annotation_path = os.path.normpath("{}/{}/{}/{}.{}".format(self.output_dir, self.destination_folder,
                                                                           self.labels_folder, name_of_file,
                                                                           self.YOLOV5YAMLExt))
            new_image_path = os.path.normpath("{}/{}/{}/{}.{}".format(self.output_dir, self.destination_folder,
                                                                      self.image_folder, name_of_file, image_extension))
            file = open(new_annotation_path, "a+")  # Create if it's not created
            file.write(line)
            file.close()
            shutil.copyfile(image_path, new_image_path)
            if int(1000 * index / length_dataset) % 100 == 0:
                print("Progress: {}%".format(int(100 * index / length_dataset)))
        print("Conversion completed of {} files!".format(length_dataset))


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv", '-c', help="File Path to the TF OD csv dataset", type=str, required=True)
    parser.add_argument("--output", '-o', help="Output folder to save the YAML dataset", type=str, required=True)
    parser.add_argument("--dir", '-d', help="Path to the dir where the dataset images are saved", type=str,
                        required=True)
    parser.add_argument("--type", '-t', help="Type of dataset (train, test, val)", type=str,
                        required=True)
    parser.add_argument('-l', "--labels", help="Classes to include", type=dict,
                        default={'car': 0, 'pedestrian': 1, 'motorcycle': 2, 'bicycle': 3, 'bus': 4, 'truck': 5})

    return parser


def main():
    args = build_argparser().parse_args()
    convertDatasetYAML = ConvertDatasetYAML(args.csv, args.output, args.dir, args.type, labels=args.labels)
    convertDatasetYAML.read_csv()
    convertDatasetYAML.convertDataset()


if __name__ == "__main__":
    main()
