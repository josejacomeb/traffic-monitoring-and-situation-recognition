#!/bin/sh
NUM_TRAIN_STEPS=10000
EXPORT_DIR=$1
NUM_TRAIN_STEPS=$2
SAMPLE_1_OF_N_EVAL_EXAMPLES=$3
PIPELINE_CONFIG_PATH=$4
NUM_EVAL_STEPS=$5
ERROR=""
if [ -z "$EXPORT_DIR" ]; then
    echo "ERROR: Please, specify an export dir"
    ERROR="1"
fi
if [ -z "$NUM_TRAIN_STEPS" ]; then
    NUM_TRAIN_STEPS=10000
    echo "INFO: Setting the default NUM_TRAIN_STEPS as $NUM_TRAIN_STEPS"
fi
if [ -z "$SAMPLE_1_OF_N_EVAL_EXAMPLES" ]; then
    SAMPLE_1_OF_N_EVAL_EXAMPLES=1
    echo "INFO: Setting the default SAMPLE_1_OF_N_EVAL_EXAMPLES to $SAMPLE_1_OF_N_EVAL_EXAMPLES"
fi
if [ -z "$PIPELINE_CONFIG_PATH" ]; then
    echo "ERROR: Please, specify the PIPELINE_CONFIG_PATH"
    ERROR="1"
fi
if [ -z "$NUM_EVAL_STEPS" ]; then
    NUM_EVAL_STEPS=1
    echo "INFO: Setting the default NUM_EVAL_STEPS as $NUM_EVAL_STEPS"
fi
if [ -n "$ERROR" ]; then
    exit 128
fi

echo $EXPORT_DIR
echo $NUM_TRAIN_STEPS
python models/research/object_detection/model_main_tf2.py \
  --model_dir=$EXPORT_DIR --num_train_steps=$NUM_TRAIN_STEPS \
  --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
  --pipeline_config_path=$PIPELINE_CONFIG_PATH \
  --sample_1_of_n_eval_on_train_examples=$NUM_EVAL_STEPS \
  --alsologtostderr
