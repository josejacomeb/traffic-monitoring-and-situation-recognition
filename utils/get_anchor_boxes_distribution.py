#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import argparse
import csv
import os
import shutil  # Copy files

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""


class AnchorBoxesDistribution:
    """
        Small Python Utility to get the anchor boxes ratio from a TF OD csv
    """

    def __init__(self, filepath, anchor_boxes, resized_width, resized_height):
        self.filepath = filepath
        self.anchor_boxes = anchor_boxes
        self.anchor_boxes_distribution = [0]*len(self.anchor_boxes)
        self.resized_width = resized_width #Size of the resized dataset if exists
        self.resized_height = resized_height #Size of the resized dataset if exists

    def read_csv(self):
        with open(self.filepath, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            total_data = 0
            for index, row in enumerate(csv_reader):
                if index > 0:
                    resize_ratio_x = 1
                    resize_ratio_y = 1
                    if self.resized_width:
                        width = float(row[1])
                        resize_ratio_x = float(self.resized_width)/width
                    if self.resized_height:
                        height = float(row[2])
                        resize_ratio_y = float(self.resized_height)/height
                    total_data += 1
                    x_min = float(row[4])
                    y_min = float(row[5])
                    x_max = float(row[6])
                    y_max = float(row[7])

                    object_width = (x_max - x_min)*resize_ratio_x
                    object_height = (y_max - y_min)*resize_ratio_y
                    ratio = object_width/object_height
                    for idx, data in enumerate(self.anchor_boxes):
                        if idx > 0:
                            if self.anchor_boxes[idx - 1] <= ratio < data:
                                self.anchor_boxes_distribution[idx] += 1
                                #print("Range: {}<{}<={}".format(self.anchor_boxes[idx - 1], ratio, data))
                        else:
                            if ratio < data:
                                self.anchor_boxes_distribution[idx] += 1
                                #print("Range: 0<{}<={}".format(ratio, data))

            print("Final anchor boxes results")
            print(self.anchor_boxes)
            print(self.anchor_boxes_distribution)
            print([percentual_anchor_distribution/total_data
                   for percentual_anchor_distribution in self.anchor_boxes_distribution])

def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv", '-c', help="File Path to the TF OD csv dataset", type=str, required=True)
    parser.add_argument("--ratios", '-r', help="Standarized ratios that will fit the anchor boxes",
                        type=list, default=[0.125, 0.25, 0.5, 0.75, 1, 2, 4, 5, 8, 10])
    parser.add_argument("--resized_width", help="In case you resize your dataset, specify the new width", default=None)
    parser.add_argument("--resized_height", help="In case you resize your dataset, specify the new height", default=None)

    return parser


def main():
    args = build_argparser().parse_args()
    getanchorboxes = AnchorBoxesDistribution(args.csv, args.ratios, args.resized_width, args.resized_height)
    getanchorboxes.read_csv()


if __name__ == "__main__":
    main()
