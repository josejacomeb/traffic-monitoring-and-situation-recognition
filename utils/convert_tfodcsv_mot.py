#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
"""
https://motchallenge.net/instructions/
Format
frame,id,x1,y1,width,height,conf,-1,-1,-1
"""

import cv2
import argparse
import csv
import os
import shutil  # Copy files