#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import csv
import random

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""


class SplitDataset:
    """
        Small Python Utility to split randomly an TF OD csv
    #TODO Save the file in the same folder as the original one
    """

    def __init__(self, csv_dataset, val, test):

        self.ratio_val_dataset = val
        self.ratio_test_dataset = test
        self.csv_dataset = csv_dataset
        self.dataset = []
        self.train_dataset = []
        self.test_dataset = []
        self.val_dataset = []
        self.fieldnames = []
        self.original_name = os.path.splitext(os.path.basename(csv_dataset))[0]
        self.dirname = os.path.dirname(csv_dataset)
        if not self.dirname == '':
            self.dirname += '/'

    def save_csv(self, data, suffix):
        with open("{}{}_{}.csv".format(self.dirname, self.original_name, suffix), mode='w') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=self.fieldnames)
            writer.writeheader()
            writer.writerows(data)

    def split_dataset(self, ratio, len_test_dataset):
        dataset = []
        #len_test_dataset = int(ratio * len(self.dataset))
        done_splitting = 0
        len_original_dataset = len(self.dataset)
        while (len_original_dataset - len(self.dataset)) < len_test_dataset:
            entries_to_remove = []
            idx = int(random.random() * (len_test_dataset - done_splitting))
            entries_to_remove.append(self.dataset[idx])
            if idx > 0:
                temp_idx = idx
                temp_filename = self.dataset[temp_idx]['filename']
                while self.dataset[temp_idx]['filename'] == temp_filename:
                    if temp_idx != idx:
                        entries_to_remove.append(self.dataset[temp_idx])
                    temp_idx -= 1
            if idx < len(self.dataset):
                temp_idx = idx
                temp_filename = self.dataset[idx]['filename']
                while self.dataset[temp_idx]['filename'] == temp_filename:
                    if temp_idx != idx:
                        entries_to_remove.append(self.dataset[temp_idx])
                    temp_idx += 1
            for entry in entries_to_remove:
                dataset.append(entry)
                self.dataset.remove(entry)
        return dataset

    def read_csv(self):
        self.dataset = []
        dataset_per_image = []
        image_name = "just_a_flag"
        with open(self.csv_dataset, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            line_count = 0
            # Split by images not by annotations
            for row in csv_reader:
                if line_count > 0:
                    if image_name == row["filename"]:
                        dataset_per_image.append(dict(row))
                    else:
                        self.dataset.append(dict(row))
                else:
                    self.fieldnames = list(row)
                line_count += 1
        length_dataset = len(self.dataset)
        self.val_dataset = self.split_dataset(self.ratio_val_dataset, int(self.ratio_val_dataset * length_dataset))
        self.val_dataset.sort(key=lambda item: item.get('filename'))
        self.test_dataset = self.split_dataset(self.ratio_test_dataset, int(self.ratio_test_dataset * length_dataset))
        self.test_dataset.sort(key=lambda item: item.get('filename'))
        for i in self.dataset:
            if type(i) == dict:
                self.train_dataset.append(i)
            else:
                self.train_dataset.extend(i)
        self.train_dataset.sort(key=lambda item: item.get('filename'))
        print("Original Size of Array: {} Size Train Array: {}, Size Val Array: {}, Size Test Array: {}".format(
                                                                                length_dataset,
                                                                                len(self.train_dataset),
                                                                                 len(self.val_dataset),
                                                                                 len(self.test_dataset)))


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", '--test', default=0.1, help="Ratio of the test set")
    parser.add_argument("-v", '--val', default=0.15, help="Ratio of the test set")
    parser.add_argument("-c", '--csv', help="Path to the csv parsed file", required=True)
    # TODO Separate the file into Train, Val, Test
    return parser


def main():
    args = build_argparser().parse_args()  # Get the args from the command line
    splitdataset = SplitDataset(args.csv, float(args.val), float(args.test))
    splitdataset.read_csv()
    splitdataset.save_csv(splitdataset.test_dataset, "test")
    splitdataset.save_csv(splitdataset.val_dataset, "val")
    splitdataset.save_csv(splitdataset.train_dataset, "train")

if __name__ == "__main__":
    main()
