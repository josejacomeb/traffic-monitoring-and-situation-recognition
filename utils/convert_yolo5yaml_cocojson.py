#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import json
import yaml

from pycococreatortools import pycococreatortools #Install https://github.com/waspinator/pycococreator
import cv2
import numpy as np
import datetime


"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""


class YOLOv5YAML2COCOjson:
    """
        Small Python Utility to convert a YOLOv5YAML to a json format
    """

    def __init__(self, yaml_file, ann_dir):
       self.yaml_file = yaml_file
       self.annotation_file_path = ann_dir
       self.val_dir = None
       self.train_dir = None
       self.test_dir = None
       self.INFO = {
            "description": "Val dataset for YOLOv5 Testing",
            "url": "https://gitlab.com/josejacomeb/traffic-monitoring-and-situation-recognition/",
            "version": "0.0.0",
            "year": 2021,
            "contributor": "josejacomeb",
            "date_created": datetime.datetime.utcnow().isoformat(' ')
       }
       self.LICENSES = [
            {
                "id": 1,
                "name": "Attribution-NonCommercial-ShareAlike License",
                "url": "http://creativecommons.org/licenses/by-nc-sa/2.0/"
            }
       ]
       self.CATEGORIES = [
        ]

    def read_csv(self):

        with open(self.yaml_file) as file:
            yaml_data = file.read()
        yaml_object = yaml.load(yaml_data, Loader=yaml.FullLoader)
        self.train_dir = yaml_object["train"]
        self.test_dir = yaml_object["test"]
        self.val_dir = yaml_object["val"]

        for idx, name in enumerate(yaml_object["names"]):
            dict_category = {'id': 0, 'name': '0', 'supercategory': "None"}
            dict_category["id"] = idx + 1
            dict_category["name"] = name
            self.CATEGORIES.append(dict_category)
        coco_output = {
            "info": self.INFO,
            "licenses": self.LICENSES,
            "categories": self.CATEGORIES,
            "images": [],
            "annotations": []
        }
        img_id = 1
        segmentation_id = 0
        for root, dirs, files in os.walk(self.annotation_file_path+"../images", topdown=False):
            for file in files:
                image_name = str(file)
                ann_name = image_name.split(".")[0] + ".txt"
                img_size = cv2.imread(self.annotation_file_path+"../images/"+image_name).shape[:2]
                image_info = pycococreatortools.create_image_info(
                    img_id, os.path.basename(image_name), img_size)
                coco_output["images"].append(image_info)
                with open("{}".format(os.path.join(self.annotation_file_path, ann_name))) as ann_file:
                    annotations = ann_file.readlines()
                    for ann in annotations:
                        yolov5_ann = ann.rstrip().split(" ")
                        category_info = {'id': int(yolov5_ann[0])+1, 'is_crowd': False}
                        img_height = img_size[0]
                        img_width = img_size[1]
                        ann_coco_x_coordinate = (float(yolov5_ann[1]) - float(yolov5_ann[3])/2) * img_width
                        ann_coco_y_coordinate = (float(yolov5_ann[2]) - float(yolov5_ann[4])/2) * img_height
                        ann_coco_width = float(yolov5_ann[3]) * img_width
                        ann_coco_height = float(yolov5_ann[4]) * img_height
                        #Because I don't have the segmentation mask, use a simple rectangle of the bbox
                        binary_mask = np.zeros(img_size, np.uint8)
                        p1 = (int(ann_coco_x_coordinate), int(ann_coco_y_coordinate))
                        p2 = (int(ann_coco_x_coordinate + ann_coco_width), int(ann_coco_y_coordinate + ann_coco_height))
                        binary_mask = cv2.rectangle(binary_mask, p1, p2, 255, -1)
                        binary_mask = (binary_mask/255).astype(np.uint8) #Set maximum value as 1
                        ann_bbox = [ann_coco_x_coordinate, ann_coco_y_coordinate, ann_coco_width, ann_coco_height]
                        annotation_info = pycococreatortools.create_annotation_info(
                            segmentation_id, img_id, category_info, binary_mask,
                            img_size, tolerance=2, bounding_box=np.array(ann_bbox))
                        if annotation_info is not None:
                            coco_output["annotations"].append(annotation_info)
                        segmentation_id += 1
                    img_id += 1
        with open('{}/instances_trafficmonitoring_train2018.json'.format(os.path.abspath("{}{}".format(self.annotation_file_path, "../" ))), 'w') as output_json_file:
            json.dump(coco_output, output_json_file)


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--yaml", '-y', help="File Path to the YAML configuration file of the dataset", type=str, required=True)
    parser.add_argument("--dir", '-d', help="Directory of the annotations", required=True)
    return parser


def main():
    args = build_argparser().parse_args()
    convertYOLOv5YAML2CocoJson = YOLOv5YAML2COCOjson(args.yaml, args.dir)
    convertYOLOv5YAML2CocoJson.read_csv()


if __name__ == "__main__":
    main()
