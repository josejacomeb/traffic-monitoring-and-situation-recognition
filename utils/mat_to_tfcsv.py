#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import hdf5storage
import argparse
import csv
import numpy as np
import cv2
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
'''
Utility to transform Matlab Mat generated from the Mathworks Image Labeler to TF OD CSV  
Got by using reverse engineering and made it emperically
Matlab 2020b, Mat file v7.3
'''


class Mat_to_TFODVCSV:

    def __init__(self, input_file_path, output_file_path):
        self.input_file_path = input_file_path
        self.output_file_path = output_file_path
        self.data = None
        self.true_input = None
        self.dataset = []

    def read_data(self):
        #TODO Show the data of the image
        #TODO show progress!
        data = hdf5storage.read(filename=self.input_file_path)
        self.true_input = data
        while self.true_input.size <= 1:
            self.true_input = self.true_input[0]
        for i in self.true_input[0]:
            if i.shape == (1,):
                self.true_input = i
                break
        while self.true_input.shape != ():
            self.true_input = self.true_input[0]
        filenames = np.array([])
        bboxes = np.array([])
        filenames_data = False
        for i in self.true_input:
            if i.shape == (1,) and not filenames_data:
                filenames = i
                filenames_data = True
            elif i.size > 1:
                bboxes = i
        while filenames.size < 2:
            filenames = filenames[0]
        list_filenames = []

        for filename in filenames:
            while type(filename) != np.str_:
                filename = filename[0]
            list_filenames.append(filename)
        for idx in range(len(bboxes)):
            classes = bboxes[idx].dtype.names
            img = cv2.imread(list_filenames[idx])
            (height, width) = img.shape[:2]
            for i in range(len(bboxes[idx][0])):
                for j in bboxes[idx][0][i]:
                    dict_dataset = {'filename': list_filenames[idx]}
                    dict_dataset['width'] = width
                    dict_dataset['height'] = height
                    dict_dataset['class'] = classes[i]
                    dict_dataset['xmin'] = int(j[0])
                    dict_dataset['ymin'] = int(j[1])
                    dict_dataset['xmax'] = int(j[2] + dict_dataset['xmin'])
                    dict_dataset['ymax'] = int(j[3] + dict_dataset['ymin'])
                    self.dataset.append(dict_dataset)

    def save_csv(self):
        # TF Object detection csv format ['filename','width','height','class','xmin','ymin','xmax','ymax'] not normalized
        with open(self.output_file_path, mode='w') as csv_file:
            fieldnames = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(self.dataset)


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser(description="Use this program to convert a Matlab's Image Labeler Mat file to "
                                                 "TensorFlow Object Detection CSV")
    parser.add_argument("-o", '--output_csv', help="Output file with the csv extension, e.g. output.csv", required=True)
    parser.add_argument("-i", '--input_mat', help="File path of the Mat File", required=True)
    return parser


def main():
    args = build_argparser().parse_args()
    mat2csv = Mat_to_TFODVCSV(args.input_mat, args.output_csv)
    mat2csv.read_data()
    mat2csv.save_csv()


if __name__ == "__main__":
    main()
