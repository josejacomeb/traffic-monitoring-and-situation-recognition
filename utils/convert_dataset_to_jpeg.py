#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import csv
import random
import os
import cv2

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
'''
Small Script to convert a dataset from any type of extension to jpeg 
'''


class convertDataset:
    """
        Small Python Utility to reduce a dataset randomly
    """

    def __init__(self, filename, filepath):
        self.dataset = []
        self.filename = filename
        self.filepath = filepath

    def read_csv(self):
        self.dataset = []
        with open(self.filename, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for index, row in enumerate(csv_reader):
                if index > 0:
                    self.dataset.append(row)

    def save_images(self):
        print("Converting the dataset to png")
        length_dataset = len(self.dataset)
        for index, dataset_file in enumerate(self.dataset):
            image_path = os.path.join(self.filepath, dataset_file[0])
            image = cv2.imread(image_path)
            new_image_path = image_path.split(".")[0] + ".jpg"
            cv2.imwrite(new_image_path, image)
            if int(1000*index/length_dataset) % 100 == 0 :
                print("Progress: {}%".format(int(100*index/length_dataset)))
        print("Conversion completed!")


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--csv', help="Path to the Dataset csv file ", required=True, default="")
    parser.add_argument('-f', '--filepath', help="Path to the Dataset", required=True, default="")
    return parser


def main():
    args = build_argparser().parse_args()  # Get the args from the command line
    if not os.path.isfile(args.csv):
        print("Please specify an appropriate input file")
        return -1
    convertdataset = convertDataset(args.csv, args.filepath)
    convertdataset.read_csv()

    convertdataset.save_images()


if __name__ == "__main__":
    main()
