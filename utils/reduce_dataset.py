#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import csv
import random
import os

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
'''
Small utility intended to reduce the number of samples of certain class by randomly deleting entries of the dataset
This utility is intended to balance the classes in your dataset
'''


class reduceDataset:
    """
        Small Python Utility to reduce a dataset randomly
    """

    def __init__(self, filename, label, number, attempts=50, output_name=None):
        self.dataset = []
        self.filename = filename
        self.label = label
        self.number = number - 1
        self.dataset_idx = []
        self.dataset = []
        self.attempts = attempts
        self.output_name = ""
        dirname = os.path.dirname(self.filename)
        if not dirname == '':
            dirname += '/'
        if output_name:
            self.output_name = output_name
        else:
            self.output_name = "{}{}_reduced.csv".format(dirname, os.path.basename(self.filename).split('.')[0])


    def read_csv(self):
        self.dataset = []
        matching_dataset_idx = []
        with open(self.filename, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for index, row in enumerate(csv_reader):
                self.dataset.append(row)
                if row[3] == self.label:
                    matching_dataset_idx.append(index)
        matching_dataset_idx.sort()  # Sort the dataset
        print("Indexes found in the class {}: {}".format(self.label, len(matching_dataset_idx)))
        progress = 0
        if len(matching_dataset_idx) >= self.number:
            if len(matching_dataset_idx) < len(self.dataset):
                actual_idx_list = len(matching_dataset_idx)
                final_components_of_class = actual_idx_list - self.number

                while actual_idx_list > final_components_of_class:
                    attempt = 0
                    max_attempt = 0
                    max_coincidences = 0
                    max_delete_matching_idxs = []
                    max_class_matching_idx = []
                    if int(10000 * (progress / self.number)) % 1000 == 0:
                        print("Progress: {} %".format(int(100 * (progress / self.number))))
                    while attempt < self.attempts:
                        list_idx = int(random.random() * (actual_idx_list))
                        idx = matching_dataset_idx[list_idx]
                        # Adding code to remove a whole image, instead of only one bbox
                        coincidences = 1
                        search_similar_idxs = idx
                        for_delete_matching_idxs = [idx]  # Whole matching idxs for an image
                        class_matching_idxs = [idx]  # Only the idx of that coincides with the class
                        max_idx = 0
                        while True:
                            search_similar_idxs -= 1
                            if search_similar_idxs >= 0:
                                if self.dataset[search_similar_idxs][0] == self.dataset[idx][0]:
                                    for_delete_matching_idxs.append(search_similar_idxs)
                                    if self.dataset[search_similar_idxs][3] == self.dataset[idx][3]:  # Same class
                                        coincidences += 1
                                        class_matching_idxs.append(search_similar_idxs)
                                else:
                                    break
                            else:
                                break
                        search_similar_idxs = idx
                        while True:
                            search_similar_idxs += 1
                            '''print("Search similiar idx: {}, idx: {}, len Dataset: {}".format(search_similar_idxs, idx,
                                                                                             len(self.dataset)))'''
                            if search_similar_idxs < len(self.dataset):
                                if self.dataset[search_similar_idxs][0] == self.dataset[idx][0]:
                                    for_delete_matching_idxs.append(search_similar_idxs)
                                    if self.dataset[search_similar_idxs][3] == self.dataset[idx][3]:
                                        coincidences += 1
                                        class_matching_idxs.append(search_similar_idxs)
                                else:
                                    break
                            else:
                                break
                        for_delete_matching_idxs.sort()
                        attempt += 1
                        '''print("Attempt: {}. Coincidences: {}/{}, Matching idxs: {}".format(attempt, coincidences,
                                                                                            len(for_delete_matching_idxs),
                                                                                            for_delete_matching_idxs))'''
                        if max_coincidences < coincidences / len(for_delete_matching_idxs):
                            max_attempt = attempt
                            max_idx = idx
                            max_coincidences = coincidences / len(for_delete_matching_idxs)
                            max_delete_matching_idxs = for_delete_matching_idxs
                            max_class_matching_idx = class_matching_idxs
                        if coincidences == len(for_delete_matching_idxs):
                            break
                        '''print("Found at attempt {}, ID: {} Coincidences: {}, idxs to remove: {}, "
                              "idxs of the same class to remove {}".format(max_attempt, max_idx, max_coincidences,
                                                                           max_delete_matching_idxs,
                                                                           max_class_matching_idx))'''
                        if max_delete_matching_idxs == []:
                            print("Dataset data: {} ".format(self.dataset[max_idx]))
                    for i in max_delete_matching_idxs:
                        self.dataset_idx.append(i)
                    for j in max_class_matching_idx:
                        matching_dataset_idx.remove(j)

                    actual_idx_list = len(matching_dataset_idx)
                    progress += len(max_class_matching_idx)
                print("{} occurrences will be removed from the dataset of the class {}".format(len(self.dataset_idx),self.label))
            else:
                print("Cannot continue, the number of entry to delete are higher than the actual number of class")
        else:
            print("Cannot continue, the number of entries to delete are higher than "
                  "the actual number of labelled objects {}".format(self.label))

    def save_csv(self):
        print("Starting the saving process")
        lists_to_remove = []
        for i in self.dataset_idx:
            lists_to_remove.append(self.dataset[i])
        length_lists_to_remove = len(lists_to_remove)
        for idx, unique_list in enumerate(lists_to_remove):
            self.dataset.remove(unique_list)
            if int(10000 * (idx / length_lists_to_remove)) % 500 == 0:
                print("Progress: {}%".format(int(100 * idx / length_lists_to_remove)))
        self.dataset_idx.clear()
        with open(self.output_name, mode='w') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerows(self.dataset)
        print("File saved as {}".format(self.output_name))


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", '--csv', help="Path to the Dataset csv file ", required=True, default="")
    parser.add_argument("-l", '--label', help="Target Class name to reduce the dataset", required=True)
    parser.add_argument("-n", '--number', help="Number of samples to reduce", type=int, required=True)
    parser.add_argument("-a", '--attempts', help="Attempts to check the class "
                                                 "with the most elements of one class", type=int, required=True)
    parser.add_argument("-o", '--output_name', help="Output name of the reduced csv file", default=None)

    return parser


def main():
    args = build_argparser().parse_args()  # Get the args from the command line
    if not os.path.isfile(args.csv):
        print("Please specify an appropriate input file")
        return -1
    reducedataset = reduceDataset(args.csv, args.label, args.number, args.attempts, args.output_name)
    reducedataset.read_csv()

    reducedataset.save_csv()


if __name__ == "__main__":
    main()
