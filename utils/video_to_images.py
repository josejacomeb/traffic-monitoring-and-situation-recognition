#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import cv2

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""


#TODO: Check if directory exists
#TODO: show percentage of completion

class VideoParser:
    def __init__(self, video_path):
        self.video_path = video_path
        self.cap = cv2.VideoCapture(self.video_path)
        self.videofps = self.cap.get(cv2.CAP_PROP_FPS)
        self.framecount = self.cap.get(cv2.CAP_PROP_FRAME_COUNT)
        if self.framecount == 0:
            raise NameError('Empty file, please convert your video or provide the right path')

    def get_frame_pos(self):
        return self.cap.get(cv2.CAP_PROP_POS_FRAMES)

    def get_frame(self):
        ret, frame = self.cap.read()
        return frame


class ImageGenerator:

    def __init__(self, path, name, extension="jpg", digits=4):
        self.path = path
        self.name = name
        self.extension = extension
        self.digits = digits

    def save_image(self, img, frame_pos):
        index = str(frame_pos)
        digits_to_complete = int(self.digits - len(index))
        index = digits_to_complete * "0" + index
        full_name_of_file = "{}{}_{}.{}".format(self.path, self.name, index, self.extension)
        cv2.imwrite(full_name_of_file, img)


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser(description="Use this program to convert a video to a sequence of images")
    parser.add_argument("-i", '--input', required=True, help="Input of the video to be converted")
    parser.add_argument("-n", '--number',
                        help="Number of images to be generated per second(it depends of the frame rate)", default=1)
    parser.add_argument("-d", '--dir', help="", default="")

    return parser


def main():
    args = build_argparser().parse_args()
    if not os.path.isfile(args.input):
        raise Exception("Please the input only can be a video file")
    cap = VideoParser(video_path=args.input)
    export_dir = ""
    head, tail = os.path.split(args.input)
    filename, file_extension = os.path.splitext(tail)
    if args.dir == "":
        export_dir = "{}/{}/".format(head, filename)
    else:
        export_dir = args.dir

    image_saver = ImageGenerator(path=export_dir, name=filename)
    frame_count = cap.framecount - 1
    fps = 1
    get_frame_every = int(cap.videofps / int(args.number))
    while cap.get_frame_pos() < frame_count:
        frame = cap.get_frame()
        if fps % get_frame_every == 0:
            image_saver.save_image(frame, int(cap.get_frame_pos()))
        if fps == cap.videofps:
            fps = 1
        fps += 1


if __name__ == "__main__":
    main()
