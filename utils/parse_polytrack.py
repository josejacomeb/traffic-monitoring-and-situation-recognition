#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3
import cv2
import argparse
import csv
import random

"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
'''
Format Data
bounding_boxes (object_id INTEGER, frame_number INTEGER, x_top_left REAL, y_top_left REAL, x_bottom_right REAL,
 y_bottom_right REAL, PRIMARY KEY(object_id, frame_number))
'''


class ParsePolytrack:
    """
        Small Python Utility to Parse a Polytrack dataset, stored in a SQLLite3 to convert it to a TF OD csv
    """

    def __init__(self, sqlite3_path="", dataset_path="", number_test_images=10,
                 label_file="datasets/UrbanTracker/labels_urbantracker.txt", default_extension=".jpg", number_of_digits=8):
        self.dataset_path = dataset_path
        self.sqlite3_path = sqlite3_path
        self.label_file = label_file
        self.number_test_images = int(number_test_images)
        self.results = {}
        self.name_tables_bboxes = "bounding_boxes"
        self.name_tables_objects = "objects"
        self.labels = []
        self.number_of_digits = number_of_digits
        self.c = None
        self.conn = None
        file = open(label_file, "r")
        for line in file:
            self.labels.append(line.strip('\n'))
        file.close()
        self.default_extension = default_extension
        self.dataset = []

    def get_data(self):
        self.conn = sqlite3.connect(self.sqlite3_path)
        self.c = self.conn.cursor()
        sql_query = "SELECT * from {}".format(self.name_tables_bboxes)
        self.c.execute(sql_query)
        results = self.c.fetchall()
        sql_query = "SELECT object_id, road_user_type from {}".format(self.name_tables_objects)
        self.c.execute(sql_query)
        # Convert from objects ID to the class ID(unknown, car, pedestrians, motorcycle, bicycle, bus,truck)
        objects_list = self.c.fetchall()
        length_results = len(results)
        ant_progress = -1
        sorted(results, key=lambda results: results[1], reverse=True)
        for idx, result in enumerate(results):
            progress = int(100 * results.index(result) / length_results)
            if progress % 10 == 0 and ant_progress != progress:
                print("======== Progress: {}% ========".format(progress))
            ant_progress = progress
            name_correction_result = "0"
            correction_number = abs(self.number_of_digits - len(str(result[1])))
            dict_dataset = {'filename': self.dataset_path + correction_number * name_correction_result + str(result[1])
                                        + self.default_extension}
            img = cv2.imread(dict_dataset['filename'])
            (height, width) = img.shape[:2]
            dict_dataset['width'] = width
            dict_dataset['height'] = height
            dict_dataset['class'] = self.labels[objects_list[result[0]][1]]
            dict_dataset['xmin'] = int(result[2])
            dict_dataset['ymin'] = int(result[3])
            dict_dataset['xmax'] = int(result[4])
            dict_dataset['ymax'] = int(result[5])
            self.dataset.append(dict_dataset)

    def test_dataset(self):
        # font
        font = cv2.FONT_HERSHEY_SIMPLEX
        # fontscale
        fontscale = 1
        # Blue color in BGR
        color = (255, 0, 0)
        iterator = 0
        # Line thickness of 2 px
        thickness = 2
        number_of_samples = len(self.dataset)
        while iterator <= self.number_test_images:
            idx = int(random.random() * number_of_samples)
            img = cv2.imread(self.dataset[idx]['filename'])
            filename = self.dataset[idx]['filename']
            bounding_boxes = []
            while True:
                if filename != self.dataset[idx]['filename']:
                    idx += 1
                    break
                idx -= 1
            while filename == self.dataset[idx]['filename']:
                start_point = (int(self.dataset[idx]['xmin']), int(self.dataset[idx]['ymin']))
                end_point = (int(self.dataset[idx]['xmax']), int(self.dataset[idx]['ymax']))
                img_class = self.dataset[idx]['class']
                bounding_boxes.append(
                    {"img_class": img_class, "start_point": start_point, "end_point": end_point})
                idx += 1
            for bbox in bounding_boxes:
                cv2.rectangle(img, bbox['start_point'], bbox['end_point'], color, thickness)
                cv2.putText(img, bbox['img_class'], bbox['start_point'], font, fontscale, color, thickness, cv2.LINE_AA)
                cv2.putText(img, filename, (20, 20), font, fontscale/2, color, thickness, cv2.LINE_AA)
            cv2.imshow("Check dataset", img)
            c = cv2.waitKey()
            iterator += 1
            if c == 27:
                break

    def save_csv(self, output_name):
        #TF Object detection csv format ['filename','width','height','class','xmin','ymin','xmax','ymax'] not normalized
        with open(output_name, mode='w') as csv_file:
            fieldnames = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(self.dataset)

    def read_csv(self, csv_filename):
        self.dataset = []
        with open(csv_filename, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            line_count = 0
            for row in csv_reader:
                if line_count > 0:
                    self.dataset.append(dict(row))
                line_count += 1


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", '--sql', help="Location of the Polytrack's sql file "
                                            "(Also you have to specify the dataset's image directory)", default="")
    parser.add_argument("-d", '--dataset', default="", help="Path to the dataset")
    parser.add_argument("-c", '--csv', help="Path to the csv parsed file", default="")
    parser.add_argument("-l", '--label', help="Path to the label file", default="datasets/UrbanTracker/labels_urbantracker.txt")
    parser.add_argument("-n", '--number', help="Number of test images to show", type=int, default=10)
    parser.add_argument("-o", '--output', help="Output name of the csv file with the extension .csv",
                        default="output.csv")
    return parser


def main():
    args = build_argparser().parse_args()  # Get the args from the command line

    parse_poly = None
    if args.sql and args.dataset:
        print("Parsing SQL file ...")
        parse_poly = ParsePolytrack(args.sql, args.dataset, number_test_images=args.number, label_file=args.label)
        parse_poly.get_data()
        parse_poly.save_csv(args.output)
    elif args.csv:
        print("Parsing pre-processed csv file ...")
        parse_poly = ParsePolytrack(label_file=args.label, number_test_images=args.number)
        parse_poly.read_csv(args.csv)
    else:
        print("Please specify the SQL file and the Dataset path to start... \n Exiting...")
        return -1
    parse_poly.test_dataset()


if __name__ == "__main__":
    main()
