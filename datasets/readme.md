# Dataset [Urban tracker](https://www.jpjodoin.com/urbantracker/)

Data format: [Polytrack](https://bitbucket.org/Nicolas/trafficintelligence/wiki/Data%20Formats)

```python
python3 models/research/object_detection/model_main_tf2.py --pipeline_config_path   models/ssd_resnet152_v1_fpn_640x640_coco17_tpu-8/pipeline.config --model_dir models/ssd_resnet152_v1_fpn_640x640_coco17_tpu-8/ --alsologtostderr --num_train_steps=150 --sample_1_of_n_eval_examples=1 --num_eval_steps=5
```
 python -m tf2onnx.convert --saved-model test_models/faster_rcnn_resnet101_v1_640x640_coco17_tpu-8/saved_model/ --output faster_ut_tf.onnx --opset 11

Deep Learning Toolbox Converter for ONNX Model Format

# Dataset Debrecen Streets

Dataset recorded by Dr. Peter Szemes

## Distribution of the Training/Eval Task
###Train
atrium - train-00000-of-00010.record   
rouen - train-00001-of-00010.record  
sherbrooke - train-00002-of-00010.record  
deb_1 - train-00003-of-00010.record  
deb_2 - train-00004-of-00010.record  
MIO-TCD - train-00005-of-00010.record  
AAU - train-00006-of-00010.record  
###Test
atrium - eval-00000-of-00010.record  
rouen - eval-00001-of-00010.record  
sherbrooke - eval-00002-of-00010.record  
deb_1 - eval-00003-of-00010.record  
deb_2 - eval-00004-of-00010.record  
MIO-TCD - eval-00005-of-00010.record  
AAU - eval-00006-of-00010.record  

