# Dataset [AAU RainSnow Traffic Surveillance Dataset](https://www.kaggle.com/aalborguniversity/aau-rainsnow)

Data format: COCO

AAU RainSnow Traffic Surveillance Dataset

## Distribution of the weather of the Scenes
### Egensevej
- Egensevej-1 - Night & Rain
- Egensevej-2 - Day & Rain
- Egensevej-3 - Night
- Egensevej-4 - Night & Snow
- Egensevej-5 - Night & Rain

### Hadsundvej
- Hadsundvej-1 - Day & Rain
- Hadsundvej-2 - Day & Rain

### Hasserisvej
- Hasserisvej-1 -  Day & Rain
- Hasserisvej-2 -  Day
- Hasserisvej-3 -  Day

### Hjorringvej
- Hjorringvej-1 - Night & Rain
- Hjorringvej-2 - Day
- Hjorringvej-3 - Night & Rain
- Hjorringvej-4 - Night & Rain

### Hobrovej
- Hobrovej-1 - Night & Rain

### Ostre
- Ostre-1 - Day & Rain
- Ostre-2 - Day & Rain
- Ostre-3 - Day & Rain
- Ostre-4 - Night & Rain

### Ringvej
- Ringvej-1 - Night
- Ringvej-2 - Night
- Ringvej-3 - Day & Rain
