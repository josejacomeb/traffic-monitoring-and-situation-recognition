#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""

import argparse
import os
import time
import logging  # Log the results

import cv2
import numpy as np

logger = logging.getLogger(__name__)
logging.basicConfig(filename='TMSR.log', format='%(filename)s %(levelname)s %(asctime)s %(message)s', level=logging.INFO)
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(filename)s %(levelname)s %(asctime)s %(message)s')
console.setFormatter(formatter)
logging.getLogger().addHandler(console)

from src.feeder import Feeder
from src.tracking import Tracker

processing_time = {"inference":[], "tracking": [], "situation_recognition": [], "fps": []}



# TODO use an index to start the video/dataset

def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", '--input', required=True,
                        help="Input file(number if camera, file path to a videofile or path to the dataset)")
    parser.add_argument("-o", '--output', default="",
                        help="If given, it will save a videofile with the result of the detection.")
    parser.add_argument("--debug", help="", type=bool, default=False)
    parser.add_argument("--headless", help="Show or not the processing", type=bool, default=False)
    parser.add_argument("--model", '-m', help="Path to a saved model(TF OD or YOLOV5)", type=str,
                        default="test_models/efficientdet_d1_coco17_tpu-32/saved_model/")
    # TODO: Add the prototxt file for the labels
    parser.add_argument("--labels", '-l', help="Label's file map.txt", type=str,
                        default="datasets/TFRecords/TMSR.pbtxt")
    parser.add_argument("--precision", '-p', help="Precision threshold for the detection", type=float,
                        default=0.5)
    parser.add_argument("--save_tracking_filename", help="Save tracking results", type=str, default="")
    parser.add_argument("--detection_every_n_frames", '-d', help="Perform object detection every n frames", default=20, type=int)
    parser.add_argument("--tracker", '-t', help="Name of the tracker to use(Currently accepted correlation, kcf, csrt)",
                        default="correlation")
    parser.add_argument("--kalman", help="Use Kalman Filtering in the tracking process", type=bool, default=True)

    return parser


def main():
    """
        Main Function of the program
    """
    args = build_argparser().parse_args()  # Get the args from the command line
    feeder = Feeder(args.input, args.output)
    if os.path.isdir(args.model):
        from src.TF_OD_Models import TF_OD
    else:
        from src.YOLOV5 import YOLOV5
    given_path = os.path.normpath(args.model)
    objectDetector = None
    if os.path.isdir(given_path):  # TF OD Model
        objectDetector = TF_OD(given_path, args.precision)
    else:
        objectDetector = YOLOV5(given_path, args.precision)

    name_of_model = objectDetector.model_name
    if args.debug:
        logger.setLevel(logging.DEBUG)
        console.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        console.setLevel(logging.INFO)
    start_time = time.time()
    objectDetector.load_model()  # Load the model
    end_time = time.time()
    elapsed_time = end_time - start_time
    logger.info("Welcome to the program!")
    logger.info("Using the {} model".format(name_of_model))
    logger.info('Time to load the model: {} s'.format(elapsed_time))

    wait = feeder.wait  # Wait to get the next frame

    pause = False  # Pause the program until the Space key is pressed
    detect_every_n_frame = args.detection_every_n_frames  # Perform detection every N Frames
    tracker = Tracker(args.tracker, dt=1/feeder.frame_rate, debug=args.debug, kalman=args.kalman)
    logger.info("Starting {} tracker".format(tracker.tracker_name))
    dict_tracked_detections = []
    fps_start_time = time.time()
    counter_fps = 0
    fps = 0
    while True:
        if not pause:
            logger.debug("========== {} ==========".format(feeder.frame_name))
            ret, image_np = feeder.get_next_frame()
        if not ret:
            logger.info("Program ended in {}".format(feeder.frame_name))
            break
        if not pause:
            if feeder.index % detect_every_n_frame == 0:
                # Use the object detector to get the bounding boxes
                start_time = time.time()
                detections = objectDetector.infer(image_np, feeder.frame_name)
                end_time = time.time()
                processing_time["inference"].append(end_time - start_time)
                time_ms = int(100 * (end_time - start_time))
                # Add bboxes to the tracker
                dict_tracked_detections = tracker.add(image_np, detections)
            else:
                # Update tracking
                start_time = time.time()
                dict_tracked_detections = tracker.update(image_np)
                end_time = time.time()
                processing_time["tracking"].append(end_time - start_time)
        if args.save_tracking_filename:
            for dtd in dict_tracked_detections:
                dtd["frame_name"] = feeder.frame_name
            tracker.save_results(dict_tracked_detections)
        if args.output or not args.headless:
            image_np_with_detections = image_np
            feeder.draw_detections(dict_tracked_detections, image_np_with_detections)
            image_np_with_detections = feeder.write_text(image_np_with_detections, "Model: {}".format(name_of_model),
                                                         (0, 30),
                                                         fontscale=0.5, color=(50, 250, 50))
            image_np_with_detections = feeder.write_text(image_np_with_detections, "FPS: {}".format(fps),
                                                         (0, feeder.height - 30), fontscale=0.5, color=(50, 250, 50))
            image_np_with_detections = feeder.write_text(image_np_with_detections, "Device: {}".format("GeForce GTX 950M"),
                                                         (feeder.width - 300, 30), fontscale=0.5, color=(50, 250, 50))
            image_np_with_detections = feeder.write_text(image_np_with_detections, "josejacomeb/UNIDEB",
                                                         (feeder.width - 300, feeder.height - 30), fontscale=0.5,
                                                         color=(50, 250, 50))
            image_np_with_detections = feeder.write_text(image_np_with_detections, feeder.frame_name,
                                                         (int(feeder.width/2), 30), fontscale=0.5,
                                                         color=(50, 250, 50))
        if args.output:
            feeder.write_video(image_np_with_detections)
        if not args.headless:
            c = cv2.waitKey(wait)
            cv2.imshow("Original Frame", image_np)
            cv2.imshow("Labeled Frame", image_np_with_detections)
            if c == 27:
                logging.info('Exiting, good bye!')
                break
            elif c == 32:  # Space bar
                pause = not pause
            elif c == 97:  # Left key
                if feeder.index > 0:
                    logger.debug(feeder.index)
                    feeder.set_index(feeder.index - 1)
                    # ret, image_np = feeder.get_next_frame()
                    manual_show = True
            elif c == 100:  # Right key
                if feeder.index > 0:
                    feeder.set_index(feeder.index + 1)
                    # ret, image_np = feeder.get_next_frame()
        counter_fps += 1
        if (time.time() - fps_start_time) > 1:
            fps = int(counter_fps/(time.time() - fps_start_time))
            processing_time["fps"].append(fps)
            fps_start_time = time.time()
            counter_fps = 0
    if args.save_tracking_filename:
        logger.info("Saving tracking data in: {}".format(args.save_tracking_filename))
        with open(args.save_tracking_filename, 'w') as file:
            for mot_line in tracker.mot_dict:
                file.write(mot_line + "\n")

    mean_inference = sum(processing_time["inference"]) / float(len(processing_time["inference"]))
    mean_tracking = sum(processing_time["tracking"]) / float(len(processing_time["tracking"]))
    #mean_situation_recognition = sum(processing_time["situation_recognition"]) / float(len(processing_time["situation_recognition"]))
    mean_fps = sum(processing_time["fps"]) / float(len(processing_time["fps"]))

    logger.info(('Average Inference time: {} second per frame'.format(mean_inference)))
    logger.info(('Average Tracking time: {} second per frame'.format(mean_tracking)))
    logger.info(('Average FPS: {} second per frame'.format(mean_fps)))
    #logger.info(('Average Mean time: {} second per frame'.format(mean_situation_recognition)))


if __name__ == "__main__":
    main()
