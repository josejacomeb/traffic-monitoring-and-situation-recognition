# Traffic Monitoring and Situation Recognition

[University of Debrecen](https://unideb.hu/en)

[Faculty of Engineering](https://eng.unideb.hu/en)


Research Project by José Jácome

## Description

Link for the [research plan](https://unidebhu-my.sharepoint.com/:w:/g/personal/josejacomeb_mailbox_unideb_hu/EUXi-_3TQ95Cvupwt5NH_AEBr1d0GOXISEqTE7hpK_znDw?e=Edch6t)

Link for the [project's references](https://unidebhu-my.sharepoint.com/:w:/g/personal/josejacomeb_mailbox_unideb_hu/EUXi-_3TQ95Cvupwt5NH_AEBr1d0GOXISEqTE7hpK_znDw?e=Edch6t) 

## Motivation

### Software Used:
- Python 3.8
- Tensorflow >= 2.2.0
- Cuda 10.2
- CUDNN 7.6.5
- Install qt5-default
Install opencv from sources


```sh
sudo pip3 -r install requirements.txt 
sudo apt-get install python3-opencv protobuf-compiler python3-tk build-essential 
conda install -c conda-forge notebook
```
For GPU users
```sh
sudo apt install nvidia-cuda-toolkit

sudo cp cuda/include/cudnn*.h /usr/include

sudo cp cuda/lib64/libcudnn* /usr/lib/x86_64-linux-gnu/

sudo chmod a+r /usr/include/cudnn*.h /usr/lib/x86_64-linux-gnu/libcudnn*
```

# CUDA related exports
```sh
export PATH=/usr/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
```
# Setup the conda environment
```sh
conda create -n tensorflow_gpu pip python=3.7
```
# Install tensorflow
```sh
pip install --upgrade tensorflow-gpu==2.2
pip install onnxruntime 
pip install -U tf2onnx
```

Using object detection API, Tutorial based on [Object Detection API](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/)
Dependencies for Anaconda
```sh
sudo apt-get install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
```
### Road Map
- [ ] Use an appropriate CCTV Dataset installed in the road to get various road situation
- [ ] Convert datasets to Tensorflow   
- [ ] Convert dataset to TFRecord Format  
- [ ] Train an object detection model with a Convolutionals Neural Networks (CNN) to detect the subjects of the road (pedestrians, car, buses, wheeled vehicles)
- [ ] Predict collision in the road via a Recurrent Neural Networks (RNN)
- [ ] Evaluate the precision of the model and the time of the prediction of an accident.  
- [ ] Study the possibility of implement the system in an embedded device

### Tests
| Model |   mAP| Dataset  | Date  | Inference Time(ms) |
|:-------:|---|---|---|---|
|SSD Models|
| efficientdet_d1_coco17_tpu-32       |  0.2374 | UrbanTracker  | 21/08/2020  | 20|   
| ssd_resnet50_v1_fpn_640x640_coco17_tpu-8  | 0.2438 | Urban Tracker | 07/09/2020 | 42|
| ssd_resnet101_v1_fpn_640x640_coco17_tpu-8 |  0.2936 | UrbanTracker  | 06/09/2020  |50|
| ssd_resnet152_v1_fpn_640x640_coco17_tpu-8 |  0.270 | UrbanTracker  | 24/08/2020  |63|
| FasterRCNN Models|
| faster_rcnn_inception_resnet_v2_640x640_coco17_tpu-8 | 0.08 | Urban Tracker | 07/09/2020 | 3.29 |
| faster_rcnn_resnet50_v1_640x640_coco17_tpu-8 | 0.3407 | Urban Tracker | 07/09/2020 | 59 |
| faster_rcnn_resnet101_v1_640x640_coco17_tpu-8 | 0.341 | UrbanTracker  | 24/08/2020  | 63 |   
| faster_rcnn_resnet152_v1_640x640_coco17_tpu-8 | 0.3204 | Urban Tracker | 07/09/2020 | 72 |

Fix libraries
https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/issues.html#export-error