#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""

import argparse
import os
import numpy as np
import time
import csv
import logging #Log the results
import random

import cv2
from src.feeder import Feeder



def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", '--input', required=True,
                        help="Input file(number if camera, file path to a videofile or path to the dataset)")
    parser.add_argument("-o", '--output', default="",
                        help="If given, it will save a videofile with the result of the detection.")
    parser.add_argument("--debug", help="", type=bool, default=False)
    parser.add_argument("--headless", help="Show or not the processing", type=bool, default=False)
    # TODO: Add the prototxt file for the labels
    parser.add_argument("--csv", '-c', help="If the input is a dataset, provide the dataset's TF OD CSV for comparison",
                        type=str, default=None, required=True)
    parser.add_argument("--ratio", '-r', help="Ratio of the total dataset to show, default=0.1",
                        type=float, default=.10)
    return parser




def main():
    """
        Program used for sanity checking the dataset
    """
    args = build_argparser().parse_args()  # Get the args from the command line
    feeder = Feeder(args.input, args.output, args.csv)
    source_name = ""
    if feeder.type == "dataset":
        dataset_path = os.path.normpath(args.input)
        source_name = "{}_{}".format(dataset_path.split(os.sep)[-2], dataset_path.split(os.sep)[-1])
    else:
        source_name = feeder.filename

    #Add a code to get only the images present in the dataset
    images_files_present = feeder.dataset_bboxes.keys()
    new_files = []
    for image_file in images_files_present:
        idx = feeder.files.index(image_file)
        new_files.append(feeder.files[idx])
    #Randomize and get a portion of the total dataset to check
    size_new_files = len(new_files)
    randomized_length = int((1 - args.ratio) * size_new_files)
    while size_new_files > randomized_length:
        size_new_files = len(new_files)
        idx = int(random.random()*(size_new_files - 1))
        new_files.pop(idx)
    feeder.files = new_files

    pause = False
    wait = 30
    basename = os.path.basename(feeder.filepath + feeder.files[feeder.index - 1])
    ret, image_np = feeder.get_next_frame()
    image_np = feeder.draw_boxes(feeder.get_bboxes(basename), image_np)


    progress = 0
    c = 0
    while True:

        basename = os.path.basename(feeder.filepath + feeder.files[feeder.index - 1])
        if pause:
            feeder.write_text(image_np, "Paused", (0 ,50))





        if c == 27:
            print("Exiting, good bye!")
            break
        elif c == 39 or c == 'a':  # Left key
            progress -= 1
            print("Left key")
            image_idx = feeder.index
            print("Left key, Img idx: {}, next idx: {}".format(image_idx, image_idx - 2))
            if (image_idx - 1) > 0:
                feeder.set_index(image_idx - 1)
            ret, image_np = feeder.get_next_frame()
            image_np = feeder.draw_boxes(feeder.get_bboxes(basename), image_np)
            print("Filename: {}, Bboxes = {}".format(basename, feeder.get_bboxes(basename)))
        elif c == 32 or c == 'd':  # Right key
            progress += 1
            image_idx = feeder.index
            print("Right key")
            print("Right key, Img idx: {}, next idx: {}".format(image_idx, image_idx + 1))
            if image_idx > 0:
                feeder.set_index(image_idx + 1)
            ret, image_np = feeder.get_next_frame()
            image_np = feeder.draw_boxes(feeder.get_bboxes(basename), image_np)
        if not ret:
            break
        c = cv2.waitKey()
        feeder.write_text(image_np, "{}/{}".format(progress, len(feeder.files)),
                          (int((feeder.width - 10)/2), 30), fontscale=0.5, color=(255, 0, 255))
        feeder.write_text(image_np, basename, (int((feeder.width - 10)/2), feeder.height - 30),fontscale=0.5, color=(0,255,0))
        cv2.imshow("Original Image", image_np)




if __name__ == "__main__":
    main()
