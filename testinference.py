#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""

import argparse
import os
import numpy as np
import time
import csv
import logging #Log the results

import cv2

from src.feeder import Feeder

elapsed = []

#TODO use an index to start the video/dataset

def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", '--input', required=True,
                        help="Input file(number if camera, file path to a videofile or path to the dataset)")
    parser.add_argument("-o", '--output', default="",
                        help="If given, it will save a videofile with the result of the detection.")
    parser.add_argument("--debug", help="", type=bool, default=False)
    parser.add_argument("--headless", help="Show or not the processing", type=bool, default=False)
    parser.add_argument("--model", '-m', help="Path to a saved model(TF OD or YOLOV5)", type=str,
                        default="test_models/efficientdet_d1_coco17_tpu-32/saved_model/")
    # TODO: Add the prototxt file for the labels
    parser.add_argument("--labels", '-l', help="Label's file map.txt", type=str,
                        default="datasets/UrbanTracker/labels_urbantracker.txt")
    parser.add_argument("--precision", '-p', help="Precision threshold for the detection", type=float,
                        default=0.5)
    parser.add_argument("--csv", '-c', help="If the input is a dataset, provide the dataset's TF OD CSV for comparison",
                        type=str, default=None)
    parser.add_argument("--savepred", '-sp', help="Save prediction results for further analysis", type=bool, default=False)
    return parser


# Load the label_map
# Load the COCO Label Map
category_index = {
    1: {'id': 1, 'name': 'car'},
    2: {'id': 2, 'name': 'pedestrians'},
    3: {'id': 3, 'name': 'motorcycle'},
    4: {'id': 4, 'name': 'bicycle'},
    5: {'id': 5, 'name': 'bus'},
    6: {'id': 6, 'name': 'truck'}
}


def main():
    """
        Main Function of the program
    """
    args = build_argparser().parse_args()  # Get the args from the command line
    feeder = Feeder(args.input, args.output, args.csv)
    if os.path.isdir(args.model):
        from src.TF_OD_Models import TF_OD
    else:
        from src.YOLOV5 import YOLOV5
    source_name = ""
    if feeder.type == "dataset":
        dataset_path = os.path.normpath(args.input)
        source_name = "{}_{}".format(dataset_path.split(os.sep)[-2], dataset_path.split(os.sep)[-1])
    else:
        source_name = feeder.filename
    given_path = os.path.normpath(args.model)
    objectDetector = None
    save_prediction = None
    if args.savepred:
        save_prediction = os.path.dirname(args.input)
        print("Saving prediction: " + save_prediction)
    if os.path.isdir(given_path): #TF OD Model
        objectDetector = TF_OD(given_path, args.precision, images_dir=save_prediction)
    else:
        objectDetector = YOLOV5(given_path, args.precision, images_dir=save_prediction)

    name_of_model = given_path.split(os.sep)[-3]
    unique_ID_job = '{}_{}'.format(source_name, name_of_model)


    if not os.path.exists("{}/".format(unique_ID_job)):
        os.makedirs(unique_ID_job)
    start_time = time.time()
    objectDetector.load_model() #Load the model
    end_time = time.time()
    elapsed_time = end_time - start_time
    print('Elapsed time: ' + str(elapsed_time) + 's')
    print('{}/{}.log'.format(unique_ID_job, "registry_log"))
    logging.basicConfig(filename='{}/{}.log'.format(unique_ID_job, "registry_log"),
                        level=logging.DEBUG)

    logging.info("Welcome to the program!")
    logging.info('Elapsed time: ' + str(elapsed_time) + 's')

    wait = feeder.wait

    pause = False
    manual_pred = False
    manual_show = False
    min_IoU_original = None
    min_IoU_comparison = None
    min_IoU_detected = None

    max_IoU_original = None
    max_IoU_comparison = None
    max_IoU_detected = None

    min_AP_original = None
    min_AP_comparison = None
    min_AP_detected = None

    max_AP_original = None
    max_AP_comparison = None
    max_AP_detected = None

    maxIoU = {"value": 0, "filename": ""}
    minIoU = {"value": 100000, "filename": ""}
    maxAP = {"value": 0, "filename": ""}
    minAP = {"value": 100000, "filename": ""}
    while True:
        if not pause or manual_pred or manual_show:
            ret, image_np = feeder.get_next_frame()
        if not ret:
            print(feeder.frame_name)
            break
        if not pause or manual_pred:
            start_time = time.time()
            detections = objectDetector.infer(image_np, feeder.frame_name)
            end_time = time.time()
            elapsed.append(end_time - start_time)
            time_ms = int(100 * (end_time - start_time))
            manual_pred = False
            print(feeder.frame_name)
        if not args.headless or args.output:
            image_np_with_detections = feeder.draw_detections(detections, image_np)
            image_np_with_detections = feeder.write_text(image_np_with_detections, "Model: {}".format(name_of_model),
                                                         (0, 30),
                                                         fontscale=0.5, color=(50, 250, 50))
            image_np_with_detections = feeder.write_text(image_np_with_detections, "Inference time: {}".format(time_ms),
                                                         (0, feeder.height - 30), fontscale=0.5, color=(50, 250, 50))
            image_np_with_detections = feeder.write_text(image_np_with_detections, "Device: {}".format("GeForce GTX 950M"),
                                                         (feeder.width - 300, 30), fontscale=0.5, color=(50, 250, 50))
            image_np_with_detections = feeder.write_text(image_np_with_detections, "josejacomeb/UNIDEB".format(time_ms),
                                                         (feeder.width - 300, feeder.height - 30), fontscale=0.5,
                                                         color=(50, 250, 50))
        if args.csv:
            bboxes = {"detection_boxes": detections['detection_boxes'][0].numpy(),
                      "detection_classes": detections['detection_classes'][0].numpy().astype(np.int32),
                      "detection_scores": detections['detection_scores'][0]}

            if not pause or manual_show:
                comparison = feeder.show_IoU(bboxes, args.precision)
                manual_show = False
        if not args.headless:
            c = cv2.waitKey(wait)
            cv2.imshow("Original Frame", image_np)
            cv2.imshow("Labeled Frame", image_np_with_detections)
            #cv2.imshow("Comparison", comparison)
            if c == 27:
                logging.info('Exiting, good bye!')
                print("Exiting, good bye!")
                break
            elif c == 32:  # Space bar
                pause = not pause
            elif c == 97:  # Left key
                if feeder.index > 0:
                    print(feeder.index)
                    feeder.set_index(feeder.index - 1)
                    #ret, image_np = feeder.get_next_frame()
                    manual_show = True
                    manual_pred = True
            elif c == 100:  # Right key
                if feeder.index > 0:
                    feeder.set_index(feeder.index + 1)
                    #ret, image_np = feeder.get_next_frame()
                    manual_show = True
                    manual_pred = True
            elif c == 115:
                feeder.write_mat("{}/{}_{}_{}.jpg".format(unique_ID_job, name_of_model, feeder.filename, "comparison"), comparison)
                feeder.write_mat("{}/{}_{}_{}.jpg".format(unique_ID_job, name_of_model, feeder.filename, "original"), image_np)
                feeder.write_mat("{}/{}_{}_{}.jpg".format(unique_ID_job, name_of_model, feeder.filename, "detected"), image_np_with_detections)

        if args.output:
            feeder.write_video(image_np_with_detections)

    mean_elapsed = sum(elapsed) / float(len(elapsed))
    print('Elapsed time: ' + str(mean_elapsed) + ' second per image')
    logging.info(('Elapsed time: ' + str(mean_elapsed) + ' second per image'))



if __name__ == "__main__":
    main()
