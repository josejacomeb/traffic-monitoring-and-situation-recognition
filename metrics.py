#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: josejacomeb
Faculty of Engineering - University of Debrecen - Hungary
"""
import os
import logging
import argparse
import json
#Temporary fix to src modules conflicts, change the name of the module in lib/python3.7/site-packages/review_object_detection_metrics-0.1.0-py3.7.egg/
#from src to review_object_detection_metrics, then change all the imports from src to review_object_detection_metrics
from src.evaluators import pascal_voc_evaluator, coco_evaluator
from src.utils import converter
from src.utils import general_utils
from src.bounding_box import BoundingBox
from src.utils.enumerators import BBFormat, BBType, MethodAveragePrecision, \
    CoordinatesType
import pandas as pd
from yolov5.utils import metrics as yolo_metrics  # Use the same YOLO metrics
import torch

import matplotlib.pyplot as plt
import numpy as np
import sklearn.metrics
import itertools


class Metrics:
    # TODO Change the name of the package
    """
        Metrics for test YOLOV5 and TF OD Models
        Using the https://github.com/rafaelpadilla/review_object_detection_metrics Libraries
        Use the conda env to run this file
    """

    def __init__(self, results_dir="/", experiment_name="output"):
        self.ground_truths = None
        self.detections = None
        self.YOLOMetrics = None
        self.dataset_classes = ['car', 'pedestrian', 'motorcycle', 'bicycle', 'bus', 'truck']
        self.cm = None
        self.results_dir = results_dir
        self.experiment_name = experiment_name
        self.experiment_path = os.path.join(self.results_dir, self.experiment_name)
        if not os.path.exists(self.experiment_path):
            os.makedirs(self.experiment_path)

    def load_annotations(self, filepath, images_dir, class_names_filepath=None, file_format="yolo"):
        filepath = os.path.abspath(filepath)
        images_dir = os.path.abspath(images_dir)
        if file_format == "yolo":
            if class_names_filepath is None:
                print("Please specify a the name of the classes in YOLOv5 format")
                return -1
            else:
                self.ground_truths = converter.yolo2bb(filepath, images_dir, class_names_filepath)
        elif file_format == "coco":
            self.ground_truths = converter.coco2bb(filepath)
        elif file_format == "cvat":
            self.ground_truths = converter.cvat2bb(filepath)
        if self.ground_truths is not None:
            print(
                "Finished loading the groundtruths {} items of the dataset in {} format".format(len(self.ground_truths),
                                                                                                file_format))

    def load_detections(self, filepath, images_dir=None, file_format="xyx2y2", ):
        filepath = os.path.abspath(filepath)
        if images_dir is not None:
            images_dir = os.path.abspath(images_dir)
        print("Filepath: {} \nImages dir: {}".format(filepath, images_dir))
        if file_format == "xyx2y2":
            print(file_format)
            self.detections = converter.text2bb(filepath, bb_type=BBType.DETECTED, bb_format=BBFormat.XYX2Y2,
                                                type_coordinates=CoordinatesType.ABSOLUTE, img_dir=images_dir)
        elif file_format == "coco":
            self.detections = converter.coco2bb(filepath, bb_type=BBType.DETECTED)
        if self.detections is not None:
            print("Finished loading the detections {} item of the dataset in {} format".format(len(self.detections),
                                                                                               file_format))
        # TODO Add more formats

    def show_bbox_distribution(self):
        dict_bbs_per_class = BoundingBox.get_amount_bounding_box_all_classes(self.ground_truths, reverse=True)
        general_utils.plot_bb_per_classes(dict_bbs_per_class, horizontally=False, rotation=90, show=True,
                                          extra_title=' (groundtruths)')

        dict_bbs_per_class = BoundingBox.get_amount_bounding_box_all_classes(self.detections, reverse=True)
        general_utils.plot_bb_per_classes(dict_bbs_per_class, horizontally=False, rotation=90, show=True,
                                          extra_title=' (detections)')

    def evaluate_precision(self):
        coco_res1 = coco_evaluator.get_coco_summary(self.ground_truths, self.detections)
        coco_res2 = coco_evaluator.get_coco_metrics(self.ground_truths, self.detections)
        print("COCO summary {}".format(coco_res1))
        print("COCO metrics: {}".format(coco_res2))
        with open(os.path.join(self.experiment_path, "coco_metrics_summary.json"), 'w') as outfile:
            json.dump(coco_res1, outfile)
        with open(os.path.join(self.experiment_path, "coco_metrics_metrics.5.json"), 'w') as outfile:
            json.dump(list(coco_res2), outfile)
        iou = 0.5
        dict_pascal_voc = pascal_voc_evaluator.get_pascalvoc_metrics(self.ground_truths, self.detections,
                                                                     iou, generate_table=True,
                                                                     method=MethodAveragePrecision.EVERY_POINT_INTERPOLATION)
        pascal_voc_evaluator.plot_precision_recall_curves(dict_pascal_voc["per_class"], showInterpolatedPrecision=True,
                                                          savePath=self.experiment_path, showAP=True)


    def generate_cm(self):
        gt_name_and_idx = []
        # Get the idxs of the gt to then group them
        for idx, gt in enumerate(self.ground_truths):
            dict_gt = {"image_name": gt.get_image_name(), "image_idx": idx}
            gt_name_and_idx.append(dict_gt)
        dt_name_and_idx = []
        # Get the idxs of the gt to then group them
        for idx, dt in enumerate(self.detections):
            dict_dt = {"image_name": dt.get_image_name(), "image_idx": idx}
            dt_name_and_idx.append(dict_dt)
        pandas_gt = pd.DataFrame(pd.DataFrame(gt_name_and_idx))
        pandas_dt = pd.DataFrame(pd.DataFrame(dt_name_and_idx))

        pandas_gt_grouped = pandas_gt.groupby(['image_name'])
        pandas_dt_grouped = pandas_dt.groupby(['image_name'])
        num_classes = len(self.dataset_classes)
        cm = yolo_metrics.ConfusionMatrix(num_classes)
        for image_name in pandas_gt_grouped.groups.keys():
            # Format for YOLO Metrics
            # labels (Array[M, 5]), class, x1, y1, x2, y2
            # detections (Array[N, 6]), x1, y1, x2, y2, conf, class
            detections_per_image = []
            gt_per_images = []
            print(image_name)
            if image_name in pandas_gt_grouped.groups.keys():
                for idx in pandas_gt_grouped.groups[image_name]:
                    gt_per_bbox = []
                    gt_per_bbox.append(self.dataset_classes.index(self.ground_truths[idx].get_class_id()))
                    gt_per_bbox.extend(self.ground_truths[idx].get_absolute_bounding_box(BBFormat.XYX2Y2))
                    gt_per_images.append(gt_per_bbox)
                    print("Idx: {} Class: {}".format(idx, self.ground_truths[idx].get_class_id()))
            else:
                detections_per_image = []
            if image_name in pandas_dt_grouped.groups.keys():
                for idx in pandas_dt_grouped.groups[image_name]:
                    dt_per_bbox = []
                    dt_per_bbox.extend(self.detections[idx].get_absolute_bounding_box(BBFormat.XYX2Y2))
                    dt_per_bbox.append(self.detections[idx].get_confidence())
                    dt_per_bbox.append(self.dataset_classes.index(self.detections[idx].get_class_id()))
                    detections_per_image.append(dt_per_bbox)
                    print("Idx: {} Class: {}".format(idx, self.detections[idx].get_class_id()))
            if len(detections_per_image) > 0:
                cm.process_batch(torch.tensor(detections_per_image), torch.tensor(gt_per_images))
        print("Plotting")
        cm.plot(self.experiment_path, self.dataset_classes)
        print("Finished plot")


def build_argparser():
    """
    Parse command line arguments.

    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-gt", '--groundtruths', required=True,
                        help="Route to the ground truths annotation foldeer")
    parser.add_argument("-d", '--detections', required=True,
                        help="Route to the detection results in any format")
    parser.add_argument("-i", '--image_dir', required=True, help="Dir of the images")
    parser.add_argument("-c", '--class_names', help="YOLO names file", default=None)
    parser.add_argument("-rd", '--results_dir', help="Dir to save the results", default="/")
    parser.add_argument("-e", '--exp_dir', help="Experiment name", default="results")

    return parser


def main():
    args = build_argparser().parse_args()
    metrics = Metrics(args.results_dir, args.exp_dir)
    metrics.load_annotations(args.groundtruths, args.image_dir, args.class_names)
    metrics.load_detections(args.detections, args.image_dir)
    metrics.evaluate_precision()
    metrics.generate_cm()


if __name__ == "__main__":
    main()
